<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacustomers = array();
	$tdatacustomers[".truncateText"] = true;
	$tdatacustomers[".NumberOfChars"] = 80;
	$tdatacustomers[".ShortName"] = "customers";
	$tdatacustomers[".OwnerID"] = "";
	$tdatacustomers[".OriginalTable"] = "customers";

//	field labels
$fieldLabelscustomers = array();
$fieldToolTipscustomers = array();
$pageTitlescustomers = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelscustomers["Portuguese(Brazil)"] = array();
	$fieldToolTipscustomers["Portuguese(Brazil)"] = array();
	$pageTitlescustomers["Portuguese(Brazil)"] = array();
	$fieldLabelscustomers["Portuguese(Brazil)"]["id"] = "Id";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["id"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["dt_nascimento"] = "Data de Nascimento";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["dt_nascimento"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["cpf"] = "CPF";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["cpf"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["titulo_eleitor"] = "Titulo de Eleitor";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["titulo_eleitor"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["zona_eleitoral"] = "Zona Eleitoral";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["zona_eleitoral"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["secao_eleitoral"] = "Secao Eleitoral";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["secao_eleitoral"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["end"] = "End";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["end"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["num"] = "Num";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["num"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["comp"] = "Comp";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["comp"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["bairro"] = "Bairro";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["bairro"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["cidade"] = "Cidade";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["cidade"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["cep"] = "Cep";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["cep"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["uf"] = "Uf";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["uf"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["fone"] = "Fone";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["fone"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["fone1"] = "Fone1";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["fone1"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["email"] = "Email";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["email"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["timestamp"] = "Timestamp";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["timestamp"] = "";
	if (count($fieldToolTipscustomers["Portuguese(Brazil)"]))
		$tdatacustomers[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscustomers[""] = array();
	$fieldToolTipscustomers[""] = array();
	$pageTitlescustomers[""] = array();
	if (count($fieldToolTipscustomers[""]))
		$tdatacustomers[".isUseToolTips"] = true;
}


	$tdatacustomers[".NCSearch"] = true;



$tdatacustomers[".shortTableName"] = "customers";
$tdatacustomers[".nSecOptions"] = 0;
$tdatacustomers[".recsPerRowList"] = 1;
$tdatacustomers[".recsPerRowPrint"] = 1;
$tdatacustomers[".mainTableOwnerID"] = "";
$tdatacustomers[".moveNext"] = 1;
$tdatacustomers[".entityType"] = 0;

$tdatacustomers[".strOriginalTableName"] = "customers";





$tdatacustomers[".showAddInPopup"] = false;

$tdatacustomers[".showEditInPopup"] = false;

$tdatacustomers[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacustomers[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacustomers[".fieldsForRegister"] = array();

$tdatacustomers[".listAjax"] = false;

	$tdatacustomers[".audit"] = true;

	$tdatacustomers[".locking"] = false;

$tdatacustomers[".edit"] = true;
$tdatacustomers[".afterEditAction"] = 1;
$tdatacustomers[".closePopupAfterEdit"] = 1;
$tdatacustomers[".afterEditActionDetTable"] = "";

$tdatacustomers[".add"] = true;
$tdatacustomers[".afterAddAction"] = 1;
$tdatacustomers[".closePopupAfterAdd"] = 1;
$tdatacustomers[".afterAddActionDetTable"] = "";

$tdatacustomers[".list"] = true;

$tdatacustomers[".view"] = true;

$tdatacustomers[".import"] = true;

$tdatacustomers[".exportTo"] = true;

$tdatacustomers[".printFriendly"] = true;

$tdatacustomers[".delete"] = true;

$tdatacustomers[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatacustomers[".searchSaving"] = false;
//

$tdatacustomers[".showSearchPanel"] = true;
		$tdatacustomers[".flexibleSearch"] = true;

if (isMobile())
	$tdatacustomers[".isUseAjaxSuggest"] = false;
else
	$tdatacustomers[".isUseAjaxSuggest"] = true;

$tdatacustomers[".rowHighlite"] = true;



$tdatacustomers[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacustomers[".isUseTimeForSearch"] = false;



$tdatacustomers[".badgeColor"] = "D2AF80";


$tdatacustomers[".allSearchFields"] = array();
$tdatacustomers[".filterFields"] = array();
$tdatacustomers[".requiredSearchFields"] = array();

$tdatacustomers[".allSearchFields"][] = "nome";
	$tdatacustomers[".allSearchFields"][] = "dt_nascimento";
	$tdatacustomers[".allSearchFields"][] = "cpf";
	$tdatacustomers[".allSearchFields"][] = "titulo_eleitor";
	$tdatacustomers[".allSearchFields"][] = "zona_eleitoral";
	$tdatacustomers[".allSearchFields"][] = "secao_eleitoral";
	$tdatacustomers[".allSearchFields"][] = "end";
	$tdatacustomers[".allSearchFields"][] = "num";
	$tdatacustomers[".allSearchFields"][] = "comp";
	$tdatacustomers[".allSearchFields"][] = "bairro";
	$tdatacustomers[".allSearchFields"][] = "cidade";
	$tdatacustomers[".allSearchFields"][] = "cep";
	$tdatacustomers[".allSearchFields"][] = "uf";
	$tdatacustomers[".allSearchFields"][] = "fone";
	$tdatacustomers[".allSearchFields"][] = "fone1";
	$tdatacustomers[".allSearchFields"][] = "email";
	$tdatacustomers[".allSearchFields"][] = "timestamp";
	

$tdatacustomers[".googleLikeFields"] = array();
$tdatacustomers[".googleLikeFields"][] = "id";
$tdatacustomers[".googleLikeFields"][] = "nome";
$tdatacustomers[".googleLikeFields"][] = "dt_nascimento";
$tdatacustomers[".googleLikeFields"][] = "cpf";
$tdatacustomers[".googleLikeFields"][] = "titulo_eleitor";
$tdatacustomers[".googleLikeFields"][] = "zona_eleitoral";
$tdatacustomers[".googleLikeFields"][] = "secao_eleitoral";
$tdatacustomers[".googleLikeFields"][] = "end";
$tdatacustomers[".googleLikeFields"][] = "num";
$tdatacustomers[".googleLikeFields"][] = "comp";
$tdatacustomers[".googleLikeFields"][] = "bairro";
$tdatacustomers[".googleLikeFields"][] = "cidade";
$tdatacustomers[".googleLikeFields"][] = "cep";
$tdatacustomers[".googleLikeFields"][] = "uf";
$tdatacustomers[".googleLikeFields"][] = "fone";
$tdatacustomers[".googleLikeFields"][] = "fone1";
$tdatacustomers[".googleLikeFields"][] = "email";
$tdatacustomers[".googleLikeFields"][] = "timestamp";


$tdatacustomers[".advSearchFields"] = array();
$tdatacustomers[".advSearchFields"][] = "id";
$tdatacustomers[".advSearchFields"][] = "nome";
$tdatacustomers[".advSearchFields"][] = "dt_nascimento";
$tdatacustomers[".advSearchFields"][] = "cpf";
$tdatacustomers[".advSearchFields"][] = "titulo_eleitor";
$tdatacustomers[".advSearchFields"][] = "zona_eleitoral";
$tdatacustomers[".advSearchFields"][] = "secao_eleitoral";
$tdatacustomers[".advSearchFields"][] = "end";
$tdatacustomers[".advSearchFields"][] = "num";
$tdatacustomers[".advSearchFields"][] = "comp";
$tdatacustomers[".advSearchFields"][] = "bairro";
$tdatacustomers[".advSearchFields"][] = "cidade";
$tdatacustomers[".advSearchFields"][] = "cep";
$tdatacustomers[".advSearchFields"][] = "uf";
$tdatacustomers[".advSearchFields"][] = "fone";
$tdatacustomers[".advSearchFields"][] = "fone1";
$tdatacustomers[".advSearchFields"][] = "email";
$tdatacustomers[".advSearchFields"][] = "timestamp";

$tdatacustomers[".tableType"] = "list";

$tdatacustomers[".printerPageOrientation"] = 0;
$tdatacustomers[".nPrinterPageScale"] = 100;

$tdatacustomers[".nPrinterSplitRecords"] = 40;

$tdatacustomers[".nPrinterPDFSplitRecords"] = 40;



$tdatacustomers[".geocodingEnabled"] = false;





$tdatacustomers[".listGridLayout"] = 3;





// view page pdf

// print page pdf


$tdatacustomers[".pageSize"] = 20;

$tdatacustomers[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacustomers[".strOrderBy"] = $tstrOrderBy;

$tdatacustomers[".orderindexes"] = array();

$tdatacustomers[".sqlHead"] = "SELECT id,  nome,  dt_nascimento,  cpf,  titulo_eleitor,  zona_eleitoral,  secao_eleitoral,  `end`,  num,  comp,  bairro,  cidade,  cep,  uf,  fone,  fone1,  email,  `timestamp`";
$tdatacustomers[".sqlFrom"] = "FROM customers";
$tdatacustomers[".sqlWhereExpr"] = "";
$tdatacustomers[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacustomers[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacustomers[".arrGroupsPerPage"] = $arrGPP;

$tdatacustomers[".highlightSearchResults"] = true;

$tableKeyscustomers = array();
$tableKeyscustomers[] = "id";
$tdatacustomers[".Keys"] = $tableKeyscustomers;

$tdatacustomers[".listFields"] = array();
$tdatacustomers[".listFields"][] = "timestamp";
$tdatacustomers[".listFields"][] = "nome";
$tdatacustomers[".listFields"][] = "dt_nascimento";
$tdatacustomers[".listFields"][] = "cpf";
$tdatacustomers[".listFields"][] = "titulo_eleitor";
$tdatacustomers[".listFields"][] = "zona_eleitoral";
$tdatacustomers[".listFields"][] = "secao_eleitoral";
$tdatacustomers[".listFields"][] = "end";
$tdatacustomers[".listFields"][] = "num";
$tdatacustomers[".listFields"][] = "comp";
$tdatacustomers[".listFields"][] = "bairro";
$tdatacustomers[".listFields"][] = "cidade";
$tdatacustomers[".listFields"][] = "cep";
$tdatacustomers[".listFields"][] = "uf";
$tdatacustomers[".listFields"][] = "fone";
$tdatacustomers[".listFields"][] = "fone1";
$tdatacustomers[".listFields"][] = "email";

$tdatacustomers[".hideMobileList"] = array();


$tdatacustomers[".viewFields"] = array();
$tdatacustomers[".viewFields"][] = "nome";
$tdatacustomers[".viewFields"][] = "dt_nascimento";
$tdatacustomers[".viewFields"][] = "cpf";
$tdatacustomers[".viewFields"][] = "titulo_eleitor";
$tdatacustomers[".viewFields"][] = "zona_eleitoral";
$tdatacustomers[".viewFields"][] = "secao_eleitoral";
$tdatacustomers[".viewFields"][] = "end";
$tdatacustomers[".viewFields"][] = "num";
$tdatacustomers[".viewFields"][] = "comp";
$tdatacustomers[".viewFields"][] = "bairro";
$tdatacustomers[".viewFields"][] = "cidade";
$tdatacustomers[".viewFields"][] = "cep";
$tdatacustomers[".viewFields"][] = "uf";
$tdatacustomers[".viewFields"][] = "fone";
$tdatacustomers[".viewFields"][] = "fone1";
$tdatacustomers[".viewFields"][] = "email";
$tdatacustomers[".viewFields"][] = "timestamp";

$tdatacustomers[".addFields"] = array();
$tdatacustomers[".addFields"][] = "nome";
$tdatacustomers[".addFields"][] = "dt_nascimento";
$tdatacustomers[".addFields"][] = "cpf";
$tdatacustomers[".addFields"][] = "titulo_eleitor";
$tdatacustomers[".addFields"][] = "zona_eleitoral";
$tdatacustomers[".addFields"][] = "secao_eleitoral";
$tdatacustomers[".addFields"][] = "end";
$tdatacustomers[".addFields"][] = "num";
$tdatacustomers[".addFields"][] = "comp";
$tdatacustomers[".addFields"][] = "bairro";
$tdatacustomers[".addFields"][] = "cidade";
$tdatacustomers[".addFields"][] = "cep";
$tdatacustomers[".addFields"][] = "uf";
$tdatacustomers[".addFields"][] = "fone";
$tdatacustomers[".addFields"][] = "fone1";
$tdatacustomers[".addFields"][] = "email";
$tdatacustomers[".addFields"][] = "timestamp";

$tdatacustomers[".masterListFields"] = array();
$tdatacustomers[".masterListFields"][] = "nome";
$tdatacustomers[".masterListFields"][] = "dt_nascimento";
$tdatacustomers[".masterListFields"][] = "cpf";
$tdatacustomers[".masterListFields"][] = "titulo_eleitor";
$tdatacustomers[".masterListFields"][] = "zona_eleitoral";
$tdatacustomers[".masterListFields"][] = "secao_eleitoral";
$tdatacustomers[".masterListFields"][] = "end";
$tdatacustomers[".masterListFields"][] = "num";
$tdatacustomers[".masterListFields"][] = "comp";
$tdatacustomers[".masterListFields"][] = "bairro";
$tdatacustomers[".masterListFields"][] = "cidade";
$tdatacustomers[".masterListFields"][] = "cep";
$tdatacustomers[".masterListFields"][] = "uf";
$tdatacustomers[".masterListFields"][] = "fone";
$tdatacustomers[".masterListFields"][] = "fone1";
$tdatacustomers[".masterListFields"][] = "email";
$tdatacustomers[".masterListFields"][] = "timestamp";

$tdatacustomers[".inlineAddFields"] = array();

$tdatacustomers[".editFields"] = array();
$tdatacustomers[".editFields"][] = "nome";
$tdatacustomers[".editFields"][] = "dt_nascimento";
$tdatacustomers[".editFields"][] = "cpf";
$tdatacustomers[".editFields"][] = "titulo_eleitor";
$tdatacustomers[".editFields"][] = "zona_eleitoral";
$tdatacustomers[".editFields"][] = "secao_eleitoral";
$tdatacustomers[".editFields"][] = "end";
$tdatacustomers[".editFields"][] = "num";
$tdatacustomers[".editFields"][] = "comp";
$tdatacustomers[".editFields"][] = "bairro";
$tdatacustomers[".editFields"][] = "cidade";
$tdatacustomers[".editFields"][] = "cep";
$tdatacustomers[".editFields"][] = "uf";
$tdatacustomers[".editFields"][] = "fone";
$tdatacustomers[".editFields"][] = "fone1";
$tdatacustomers[".editFields"][] = "email";
$tdatacustomers[".editFields"][] = "timestamp";

$tdatacustomers[".inlineEditFields"] = array();

$tdatacustomers[".exportFields"] = array();
$tdatacustomers[".exportFields"][] = "nome";
$tdatacustomers[".exportFields"][] = "dt_nascimento";
$tdatacustomers[".exportFields"][] = "cpf";
$tdatacustomers[".exportFields"][] = "titulo_eleitor";
$tdatacustomers[".exportFields"][] = "zona_eleitoral";
$tdatacustomers[".exportFields"][] = "secao_eleitoral";
$tdatacustomers[".exportFields"][] = "end";
$tdatacustomers[".exportFields"][] = "num";
$tdatacustomers[".exportFields"][] = "comp";
$tdatacustomers[".exportFields"][] = "bairro";
$tdatacustomers[".exportFields"][] = "cidade";
$tdatacustomers[".exportFields"][] = "cep";
$tdatacustomers[".exportFields"][] = "uf";
$tdatacustomers[".exportFields"][] = "fone";
$tdatacustomers[".exportFields"][] = "fone1";
$tdatacustomers[".exportFields"][] = "email";
$tdatacustomers[".exportFields"][] = "timestamp";

$tdatacustomers[".importFields"] = array();
$tdatacustomers[".importFields"][] = "nome";
$tdatacustomers[".importFields"][] = "dt_nascimento";
$tdatacustomers[".importFields"][] = "cpf";
$tdatacustomers[".importFields"][] = "titulo_eleitor";
$tdatacustomers[".importFields"][] = "zona_eleitoral";
$tdatacustomers[".importFields"][] = "secao_eleitoral";
$tdatacustomers[".importFields"][] = "end";
$tdatacustomers[".importFields"][] = "num";
$tdatacustomers[".importFields"][] = "comp";
$tdatacustomers[".importFields"][] = "bairro";
$tdatacustomers[".importFields"][] = "cidade";
$tdatacustomers[".importFields"][] = "cep";
$tdatacustomers[".importFields"][] = "uf";
$tdatacustomers[".importFields"][] = "fone";
$tdatacustomers[".importFields"][] = "fone1";
$tdatacustomers[".importFields"][] = "email";
$tdatacustomers[".importFields"][] = "timestamp";

$tdatacustomers[".printFields"] = array();
$tdatacustomers[".printFields"][] = "nome";
$tdatacustomers[".printFields"][] = "dt_nascimento";
$tdatacustomers[".printFields"][] = "cpf";
$tdatacustomers[".printFields"][] = "titulo_eleitor";
$tdatacustomers[".printFields"][] = "zona_eleitoral";
$tdatacustomers[".printFields"][] = "secao_eleitoral";
$tdatacustomers[".printFields"][] = "end";
$tdatacustomers[".printFields"][] = "num";
$tdatacustomers[".printFields"][] = "comp";
$tdatacustomers[".printFields"][] = "bairro";
$tdatacustomers[".printFields"][] = "cidade";
$tdatacustomers[".printFields"][] = "cep";
$tdatacustomers[".printFields"][] = "uf";
$tdatacustomers[".printFields"][] = "fone";
$tdatacustomers[".printFields"][] = "fone1";
$tdatacustomers[".printFields"][] = "email";
$tdatacustomers[".printFields"][] = "timestamp";

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["id"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["nome"] = $fdata;
//	dt_nascimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "dt_nascimento";
	$fdata["GoodName"] = "dt_nascimento";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","dt_nascimento");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "dt_nascimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dt_nascimento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["dt_nascimento"] = $fdata;
//	cpf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "cpf";
	$fdata["GoodName"] = "cpf";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","cpf");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cpf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cpf";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["cpf"] = $fdata;
//	titulo_eleitor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "titulo_eleitor";
	$fdata["GoodName"] = "titulo_eleitor";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","titulo_eleitor");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "titulo_eleitor";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "titulo_eleitor";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["titulo_eleitor"] = $fdata;
//	zona_eleitoral
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "zona_eleitoral";
	$fdata["GoodName"] = "zona_eleitoral";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","zona_eleitoral");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "zona_eleitoral";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "zona_eleitoral";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["zona_eleitoral"] = $fdata;
//	secao_eleitoral
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "secao_eleitoral";
	$fdata["GoodName"] = "secao_eleitoral";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","secao_eleitoral");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "secao_eleitoral";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "secao_eleitoral";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["secao_eleitoral"] = $fdata;
//	end
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "end";
	$fdata["GoodName"] = "end";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","end");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "end";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`end`";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["end"] = $fdata;
//	num
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "num";
	$fdata["GoodName"] = "num";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","num");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "num";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "num";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["num"] = $fdata;
//	comp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "comp";
	$fdata["GoodName"] = "comp";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","comp");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "comp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "comp";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["comp"] = $fdata;
//	bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "bairro";
	$fdata["GoodName"] = "bairro";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","bairro");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "bairro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["bairro"] = $fdata;
//	cidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "cidade";
	$fdata["GoodName"] = "cidade";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","cidade");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cidade";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["cidade"] = $fdata;
//	cep
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "cep";
	$fdata["GoodName"] = "cep";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","cep");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cep";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cep";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["cep"] = $fdata;
//	uf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "uf";
	$fdata["GoodName"] = "uf";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","uf");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "uf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "uf";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["uf"] = $fdata;
//	fone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "fone";
	$fdata["GoodName"] = "fone";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","fone");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "fone";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fone";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["fone"] = $fdata;
//	fone1
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "fone1";
	$fdata["GoodName"] = "fone1";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","fone1");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "fone1";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fone1";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["fone1"] = $fdata;
//	email
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "email";
	$fdata["GoodName"] = "email";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","email");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "email";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "email";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["email"] = $fdata;
//	timestamp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "timestamp";
	$fdata["GoodName"] = "timestamp";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","timestamp");
	$fdata["FieldType"] = 135;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "timestamp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`timestamp`";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatacustomers["timestamp"] = $fdata;


$tables_data["customers"]=&$tdatacustomers;
$field_labels["customers"] = &$fieldLabelscustomers;
$fieldToolTips["customers"] = &$fieldToolTipscustomers;
$page_titles["customers"] = &$pageTitlescustomers;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["customers"] = array();
//	verifica_voucher
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="verifica_voucher";
		$detailsParam["dOriginalTable"] = "verifica_voucher";
		$detailsParam["proceedLink"] = true;
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "verifica_voucher";
	$detailsParam["dCaptionTable"] = GetTableCaption("verifica_voucher");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();

/*			$detailsParam["dispChildCount"] = 0;
	*/
	$detailsParam["dispChildCount"] = "1";
	
		$detailsParam["hideChild"] = false;
			$detailsParam["previewOnList"] = "1";
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
		
	$detailsTablesData["customers"][$dIndex] = $detailsParam;

	
		$detailsTablesData["customers"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["customers"][$dIndex]["masterKeys"][]="cpf";

				$detailsTablesData["customers"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["customers"][$dIndex]["detailKeys"][]="cpf";

// tables which are master tables for current table (detail)
$masterTablesData["customers"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_customers()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  nome,  dt_nascimento,  cpf,  titulo_eleitor,  zona_eleitoral,  secao_eleitoral,  `end`,  num,  comp,  bairro,  cidade,  cep,  uf,  fone,  fone1,  email,  `timestamp`";
$proto0["m_strFrom"] = "FROM customers";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "customers";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto8["m_sql"] = "nome";
$proto8["m_srcTableName"] = "customers";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "dt_nascimento",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto10["m_sql"] = "dt_nascimento";
$proto10["m_srcTableName"] = "customers";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "cpf",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto12["m_sql"] = "cpf";
$proto12["m_srcTableName"] = "customers";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "titulo_eleitor",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto14["m_sql"] = "titulo_eleitor";
$proto14["m_srcTableName"] = "customers";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "zona_eleitoral",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto16["m_sql"] = "zona_eleitoral";
$proto16["m_srcTableName"] = "customers";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "secao_eleitoral",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto18["m_sql"] = "secao_eleitoral";
$proto18["m_srcTableName"] = "customers";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "end",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto20["m_sql"] = "`end`";
$proto20["m_srcTableName"] = "customers";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "num",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto22["m_sql"] = "num";
$proto22["m_srcTableName"] = "customers";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "comp",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto24["m_sql"] = "comp";
$proto24["m_srcTableName"] = "customers";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "bairro",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto26["m_sql"] = "bairro";
$proto26["m_srcTableName"] = "customers";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "cidade",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto28["m_sql"] = "cidade";
$proto28["m_srcTableName"] = "customers";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "cep",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto30["m_sql"] = "cep";
$proto30["m_srcTableName"] = "customers";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "uf",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto32["m_sql"] = "uf";
$proto32["m_srcTableName"] = "customers";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "fone",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto34["m_sql"] = "fone";
$proto34["m_srcTableName"] = "customers";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "fone1",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto36["m_sql"] = "fone1";
$proto36["m_srcTableName"] = "customers";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "email",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto38["m_sql"] = "email";
$proto38["m_srcTableName"] = "customers";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "timestamp",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto40["m_sql"] = "`timestamp`";
$proto40["m_srcTableName"] = "customers";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto42=array();
$proto42["m_link"] = "SQLL_MAIN";
			$proto43=array();
$proto43["m_strName"] = "customers";
$proto43["m_srcTableName"] = "customers";
$proto43["m_columns"] = array();
$proto43["m_columns"][] = "id";
$proto43["m_columns"][] = "nome";
$proto43["m_columns"][] = "dt_nascimento";
$proto43["m_columns"][] = "cpf";
$proto43["m_columns"][] = "titulo_eleitor";
$proto43["m_columns"][] = "zona_eleitoral";
$proto43["m_columns"][] = "secao_eleitoral";
$proto43["m_columns"][] = "end";
$proto43["m_columns"][] = "num";
$proto43["m_columns"][] = "comp";
$proto43["m_columns"][] = "bairro";
$proto43["m_columns"][] = "cidade";
$proto43["m_columns"][] = "cep";
$proto43["m_columns"][] = "uf";
$proto43["m_columns"][] = "fone";
$proto43["m_columns"][] = "fone1";
$proto43["m_columns"][] = "email";
$proto43["m_columns"][] = "timestamp";
$obj = new SQLTable($proto43);

$proto42["m_table"] = $obj;
$proto42["m_sql"] = "customers";
$proto42["m_alias"] = "";
$proto42["m_srcTableName"] = "customers";
$proto44=array();
$proto44["m_sql"] = "";
$proto44["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto44["m_column"]=$obj;
$proto44["m_contained"] = array();
$proto44["m_strCase"] = "";
$proto44["m_havingmode"] = false;
$proto44["m_inBrackets"] = false;
$proto44["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto44);

$proto42["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto42);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="customers";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_customers = createSqlQuery_customers();


	
		;

																		

$tdatacustomers[".sqlquery"] = $queryData_customers;

$tableEvents["customers"] = new eventsBase;
$tdatacustomers[".hasEvents"] = false;

?>