<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacustomers_add = array();
	$tdatacustomers_add[".truncateText"] = true;
	$tdatacustomers_add[".NumberOfChars"] = 80;
	$tdatacustomers_add[".ShortName"] = "customers_add";
	$tdatacustomers_add[".OwnerID"] = "";
	$tdatacustomers_add[".OriginalTable"] = "customers";

//	field labels
$fieldLabelscustomers_add = array();
$fieldToolTipscustomers_add = array();
$pageTitlescustomers_add = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelscustomers_add["Portuguese(Brazil)"] = array();
	$fieldToolTipscustomers_add["Portuguese(Brazil)"] = array();
	$pageTitlescustomers_add["Portuguese(Brazil)"] = array();
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["id"] = "Id";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["id"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["dt_nascimento"] = "Data de Nascimento";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["dt_nascimento"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["cpf"] = "CPF";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["cpf"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["titulo_eleitor"] = "Titulo de Eleitor";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["titulo_eleitor"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["zona_eleitoral"] = "Zona Eleitoral";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["zona_eleitoral"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["secao_eleitoral"] = "Secao Eleitoral";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["secao_eleitoral"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["end"] = "End";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["end"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["num"] = "Num";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["num"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["comp"] = "Comp";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["comp"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["bairro"] = "Bairro";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["bairro"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["cidade"] = "Cidade";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["cidade"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["cep"] = "Cep";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["cep"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["uf"] = "Uf";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["uf"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["fone"] = "Fone";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["fone"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["fone1"] = "Fone1";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["fone1"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["email"] = "Email";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["email"] = "";
	$fieldLabelscustomers_add["Portuguese(Brazil)"]["timestamp"] = "Timestamp";
	$fieldToolTipscustomers_add["Portuguese(Brazil)"]["timestamp"] = "";
	if (count($fieldToolTipscustomers_add["Portuguese(Brazil)"]))
		$tdatacustomers_add[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscustomers_add[""] = array();
	$fieldToolTipscustomers_add[""] = array();
	$pageTitlescustomers_add[""] = array();
	if (count($fieldToolTipscustomers_add[""]))
		$tdatacustomers_add[".isUseToolTips"] = true;
}


	$tdatacustomers_add[".NCSearch"] = true;



$tdatacustomers_add[".shortTableName"] = "customers_add";
$tdatacustomers_add[".nSecOptions"] = 0;
$tdatacustomers_add[".recsPerRowList"] = 1;
$tdatacustomers_add[".recsPerRowPrint"] = 1;
$tdatacustomers_add[".mainTableOwnerID"] = "";
$tdatacustomers_add[".moveNext"] = 1;
$tdatacustomers_add[".entityType"] = 1;

$tdatacustomers_add[".strOriginalTableName"] = "customers";





$tdatacustomers_add[".showAddInPopup"] = false;

$tdatacustomers_add[".showEditInPopup"] = false;

$tdatacustomers_add[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacustomers_add[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacustomers_add[".fieldsForRegister"] = array();

$tdatacustomers_add[".listAjax"] = false;

	$tdatacustomers_add[".audit"] = true;

	$tdatacustomers_add[".locking"] = false;


$tdatacustomers_add[".add"] = true;
$tdatacustomers_add[".afterAddAction"] = 1;
$tdatacustomers_add[".closePopupAfterAdd"] = 1;
$tdatacustomers_add[".afterAddActionDetTable"] = "";







$tdatacustomers_add[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatacustomers_add[".searchSaving"] = false;
//

$tdatacustomers_add[".showSearchPanel"] = true;
		$tdatacustomers_add[".flexibleSearch"] = true;

if (isMobile())
	$tdatacustomers_add[".isUseAjaxSuggest"] = false;
else
	$tdatacustomers_add[".isUseAjaxSuggest"] = true;

$tdatacustomers_add[".rowHighlite"] = true;



$tdatacustomers_add[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacustomers_add[".isUseTimeForSearch"] = false;



$tdatacustomers_add[".badgeColor"] = "D2691E";


$tdatacustomers_add[".allSearchFields"] = array();
$tdatacustomers_add[".filterFields"] = array();
$tdatacustomers_add[".requiredSearchFields"] = array();



$tdatacustomers_add[".googleLikeFields"] = array();
$tdatacustomers_add[".googleLikeFields"][] = "id";
$tdatacustomers_add[".googleLikeFields"][] = "nome";
$tdatacustomers_add[".googleLikeFields"][] = "dt_nascimento";
$tdatacustomers_add[".googleLikeFields"][] = "cpf";
$tdatacustomers_add[".googleLikeFields"][] = "titulo_eleitor";
$tdatacustomers_add[".googleLikeFields"][] = "zona_eleitoral";
$tdatacustomers_add[".googleLikeFields"][] = "secao_eleitoral";
$tdatacustomers_add[".googleLikeFields"][] = "end";
$tdatacustomers_add[".googleLikeFields"][] = "num";
$tdatacustomers_add[".googleLikeFields"][] = "comp";
$tdatacustomers_add[".googleLikeFields"][] = "bairro";
$tdatacustomers_add[".googleLikeFields"][] = "cidade";
$tdatacustomers_add[".googleLikeFields"][] = "cep";
$tdatacustomers_add[".googleLikeFields"][] = "uf";
$tdatacustomers_add[".googleLikeFields"][] = "fone";
$tdatacustomers_add[".googleLikeFields"][] = "fone1";
$tdatacustomers_add[".googleLikeFields"][] = "email";
$tdatacustomers_add[".googleLikeFields"][] = "timestamp";


$tdatacustomers_add[".advSearchFields"] = array();
$tdatacustomers_add[".advSearchFields"][] = "id";
$tdatacustomers_add[".advSearchFields"][] = "timestamp";
$tdatacustomers_add[".advSearchFields"][] = "nome";
$tdatacustomers_add[".advSearchFields"][] = "dt_nascimento";
$tdatacustomers_add[".advSearchFields"][] = "cpf";
$tdatacustomers_add[".advSearchFields"][] = "titulo_eleitor";
$tdatacustomers_add[".advSearchFields"][] = "zona_eleitoral";
$tdatacustomers_add[".advSearchFields"][] = "secao_eleitoral";
$tdatacustomers_add[".advSearchFields"][] = "end";
$tdatacustomers_add[".advSearchFields"][] = "num";
$tdatacustomers_add[".advSearchFields"][] = "comp";
$tdatacustomers_add[".advSearchFields"][] = "bairro";
$tdatacustomers_add[".advSearchFields"][] = "cidade";
$tdatacustomers_add[".advSearchFields"][] = "cep";
$tdatacustomers_add[".advSearchFields"][] = "uf";
$tdatacustomers_add[".advSearchFields"][] = "fone";
$tdatacustomers_add[".advSearchFields"][] = "fone1";
$tdatacustomers_add[".advSearchFields"][] = "email";

$tdatacustomers_add[".tableType"] = "list";

$tdatacustomers_add[".printerPageOrientation"] = 0;
$tdatacustomers_add[".nPrinterPageScale"] = 100;

$tdatacustomers_add[".nPrinterSplitRecords"] = 40;

$tdatacustomers_add[".nPrinterPDFSplitRecords"] = 40;



$tdatacustomers_add[".geocodingEnabled"] = false;





$tdatacustomers_add[".listGridLayout"] = 3;





// view page pdf

// print page pdf


$tdatacustomers_add[".pageSize"] = 20;

$tdatacustomers_add[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacustomers_add[".strOrderBy"] = $tstrOrderBy;

$tdatacustomers_add[".orderindexes"] = array();

$tdatacustomers_add[".sqlHead"] = "SELECT id,  nome,  dt_nascimento,  cpf,  titulo_eleitor,  zona_eleitoral,  secao_eleitoral,  `end`,  num,  comp,  bairro,  cidade,  cep,  uf,  fone,  fone1,  email,  `timestamp`";
$tdatacustomers_add[".sqlFrom"] = "FROM customers";
$tdatacustomers_add[".sqlWhereExpr"] = "";
$tdatacustomers_add[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacustomers_add[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacustomers_add[".arrGroupsPerPage"] = $arrGPP;

$tdatacustomers_add[".highlightSearchResults"] = true;

$tableKeyscustomers_add = array();
$tableKeyscustomers_add[] = "id";
$tdatacustomers_add[".Keys"] = $tableKeyscustomers_add;

$tdatacustomers_add[".listFields"] = array();

$tdatacustomers_add[".hideMobileList"] = array();


$tdatacustomers_add[".viewFields"] = array();

$tdatacustomers_add[".addFields"] = array();
$tdatacustomers_add[".addFields"][] = "timestamp";
$tdatacustomers_add[".addFields"][] = "nome";
$tdatacustomers_add[".addFields"][] = "dt_nascimento";
$tdatacustomers_add[".addFields"][] = "cpf";
$tdatacustomers_add[".addFields"][] = "titulo_eleitor";
$tdatacustomers_add[".addFields"][] = "zona_eleitoral";
$tdatacustomers_add[".addFields"][] = "secao_eleitoral";
$tdatacustomers_add[".addFields"][] = "end";
$tdatacustomers_add[".addFields"][] = "num";
$tdatacustomers_add[".addFields"][] = "comp";
$tdatacustomers_add[".addFields"][] = "bairro";
$tdatacustomers_add[".addFields"][] = "cidade";
$tdatacustomers_add[".addFields"][] = "cep";
$tdatacustomers_add[".addFields"][] = "uf";
$tdatacustomers_add[".addFields"][] = "fone";
$tdatacustomers_add[".addFields"][] = "fone1";
$tdatacustomers_add[".addFields"][] = "email";

$tdatacustomers_add[".masterListFields"] = array();
$tdatacustomers_add[".masterListFields"][] = "id";
$tdatacustomers_add[".masterListFields"][] = "timestamp";
$tdatacustomers_add[".masterListFields"][] = "nome";
$tdatacustomers_add[".masterListFields"][] = "dt_nascimento";
$tdatacustomers_add[".masterListFields"][] = "cpf";
$tdatacustomers_add[".masterListFields"][] = "titulo_eleitor";
$tdatacustomers_add[".masterListFields"][] = "zona_eleitoral";
$tdatacustomers_add[".masterListFields"][] = "secao_eleitoral";
$tdatacustomers_add[".masterListFields"][] = "end";
$tdatacustomers_add[".masterListFields"][] = "num";
$tdatacustomers_add[".masterListFields"][] = "comp";
$tdatacustomers_add[".masterListFields"][] = "bairro";
$tdatacustomers_add[".masterListFields"][] = "cidade";
$tdatacustomers_add[".masterListFields"][] = "cep";
$tdatacustomers_add[".masterListFields"][] = "uf";
$tdatacustomers_add[".masterListFields"][] = "fone";
$tdatacustomers_add[".masterListFields"][] = "fone1";
$tdatacustomers_add[".masterListFields"][] = "email";

$tdatacustomers_add[".inlineAddFields"] = array();

$tdatacustomers_add[".editFields"] = array();

$tdatacustomers_add[".inlineEditFields"] = array();

$tdatacustomers_add[".exportFields"] = array();

$tdatacustomers_add[".importFields"] = array();

$tdatacustomers_add[".printFields"] = array();

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["id"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["nome"] = $fdata;
//	dt_nascimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "dt_nascimento";
	$fdata["GoodName"] = "dt_nascimento";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","dt_nascimento");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "dt_nascimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dt_nascimento";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["dt_nascimento"] = $fdata;
//	cpf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "cpf";
	$fdata["GoodName"] = "cpf";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","cpf");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "cpf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cpf";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["cpf"] = $fdata;
//	titulo_eleitor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "titulo_eleitor";
	$fdata["GoodName"] = "titulo_eleitor";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","titulo_eleitor");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "titulo_eleitor";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "titulo_eleitor";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["titulo_eleitor"] = $fdata;
//	zona_eleitoral
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "zona_eleitoral";
	$fdata["GoodName"] = "zona_eleitoral";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","zona_eleitoral");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "zona_eleitoral";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "zona_eleitoral";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["zona_eleitoral"] = $fdata;
//	secao_eleitoral
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "secao_eleitoral";
	$fdata["GoodName"] = "secao_eleitoral";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","secao_eleitoral");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "secao_eleitoral";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "secao_eleitoral";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["secao_eleitoral"] = $fdata;
//	end
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "end";
	$fdata["GoodName"] = "end";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","end");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "end";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`end`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["end"] = $fdata;
//	num
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "num";
	$fdata["GoodName"] = "num";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","num");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "num";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "num";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["num"] = $fdata;
//	comp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "comp";
	$fdata["GoodName"] = "comp";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","comp");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "comp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "comp";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["comp"] = $fdata;
//	bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "bairro";
	$fdata["GoodName"] = "bairro";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","bairro");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "bairro";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["bairro"] = $fdata;
//	cidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "cidade";
	$fdata["GoodName"] = "cidade";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","cidade");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "cidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cidade";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["cidade"] = $fdata;
//	cep
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "cep";
	$fdata["GoodName"] = "cep";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","cep");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "cep";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cep";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["cep"] = $fdata;
//	uf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "uf";
	$fdata["GoodName"] = "uf";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","uf");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "uf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "uf";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["uf"] = $fdata;
//	fone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "fone";
	$fdata["GoodName"] = "fone";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","fone");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "fone";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fone";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["fone"] = $fdata;
//	fone1
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "fone1";
	$fdata["GoodName"] = "fone1";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","fone1");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "fone1";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fone1";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["fone1"] = $fdata;
//	email
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "email";
	$fdata["GoodName"] = "email";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","email");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "email";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "email";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["email"] = $fdata;
//	timestamp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "timestamp";
	$fdata["GoodName"] = "timestamp";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers_add","timestamp");
	$fdata["FieldType"] = 135;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "timestamp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`timestamp`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers_add["timestamp"] = $fdata;


$tables_data["customers_add"]=&$tdatacustomers_add;
$field_labels["customers_add"] = &$fieldLabelscustomers_add;
$fieldToolTips["customers_add"] = &$fieldToolTipscustomers_add;
$page_titles["customers_add"] = &$pageTitlescustomers_add;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["customers_add"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["customers_add"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_customers_add()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  nome,  dt_nascimento,  cpf,  titulo_eleitor,  zona_eleitoral,  secao_eleitoral,  `end`,  num,  comp,  bairro,  cidade,  cep,  uf,  fone,  fone1,  email,  `timestamp`";
$proto0["m_strFrom"] = "FROM customers";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "customers_add";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto8["m_sql"] = "nome";
$proto8["m_srcTableName"] = "customers_add";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "dt_nascimento",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto10["m_sql"] = "dt_nascimento";
$proto10["m_srcTableName"] = "customers_add";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "cpf",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto12["m_sql"] = "cpf";
$proto12["m_srcTableName"] = "customers_add";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "titulo_eleitor",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto14["m_sql"] = "titulo_eleitor";
$proto14["m_srcTableName"] = "customers_add";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "zona_eleitoral",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto16["m_sql"] = "zona_eleitoral";
$proto16["m_srcTableName"] = "customers_add";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "secao_eleitoral",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto18["m_sql"] = "secao_eleitoral";
$proto18["m_srcTableName"] = "customers_add";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "end",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto20["m_sql"] = "`end`";
$proto20["m_srcTableName"] = "customers_add";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "num",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto22["m_sql"] = "num";
$proto22["m_srcTableName"] = "customers_add";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "comp",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto24["m_sql"] = "comp";
$proto24["m_srcTableName"] = "customers_add";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "bairro",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto26["m_sql"] = "bairro";
$proto26["m_srcTableName"] = "customers_add";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "cidade",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto28["m_sql"] = "cidade";
$proto28["m_srcTableName"] = "customers_add";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "cep",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto30["m_sql"] = "cep";
$proto30["m_srcTableName"] = "customers_add";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "uf",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto32["m_sql"] = "uf";
$proto32["m_srcTableName"] = "customers_add";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "fone",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto34["m_sql"] = "fone";
$proto34["m_srcTableName"] = "customers_add";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "fone1",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto36["m_sql"] = "fone1";
$proto36["m_srcTableName"] = "customers_add";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "email",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto38["m_sql"] = "email";
$proto38["m_srcTableName"] = "customers_add";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "timestamp",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers_add"
));

$proto40["m_sql"] = "`timestamp`";
$proto40["m_srcTableName"] = "customers_add";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto42=array();
$proto42["m_link"] = "SQLL_MAIN";
			$proto43=array();
$proto43["m_strName"] = "customers";
$proto43["m_srcTableName"] = "customers_add";
$proto43["m_columns"] = array();
$proto43["m_columns"][] = "id";
$proto43["m_columns"][] = "nome";
$proto43["m_columns"][] = "dt_nascimento";
$proto43["m_columns"][] = "cpf";
$proto43["m_columns"][] = "titulo_eleitor";
$proto43["m_columns"][] = "zona_eleitoral";
$proto43["m_columns"][] = "secao_eleitoral";
$proto43["m_columns"][] = "end";
$proto43["m_columns"][] = "num";
$proto43["m_columns"][] = "comp";
$proto43["m_columns"][] = "bairro";
$proto43["m_columns"][] = "cidade";
$proto43["m_columns"][] = "cep";
$proto43["m_columns"][] = "uf";
$proto43["m_columns"][] = "fone";
$proto43["m_columns"][] = "fone1";
$proto43["m_columns"][] = "email";
$proto43["m_columns"][] = "timestamp";
$obj = new SQLTable($proto43);

$proto42["m_table"] = $obj;
$proto42["m_sql"] = "customers";
$proto42["m_alias"] = "";
$proto42["m_srcTableName"] = "customers_add";
$proto44=array();
$proto44["m_sql"] = "";
$proto44["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto44["m_column"]=$obj;
$proto44["m_contained"] = array();
$proto44["m_strCase"] = "";
$proto44["m_havingmode"] = false;
$proto44["m_inBrackets"] = false;
$proto44["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto44);

$proto42["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto42);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="customers_add";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_customers_add = createSqlQuery_customers_add();


	
		;

																		

$tdatacustomers_add[".sqlquery"] = $queryData_customers_add;

$tableEvents["customers_add"] = new eventsBase;
$tdatacustomers_add[".hasEvents"] = false;

?>