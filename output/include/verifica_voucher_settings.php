<?php
require_once(getabspath("classes/cipherer.php"));




$tdataverifica_voucher = array();
	$tdataverifica_voucher[".truncateText"] = true;
	$tdataverifica_voucher[".NumberOfChars"] = 80;
	$tdataverifica_voucher[".ShortName"] = "verifica_voucher";
	$tdataverifica_voucher[".OwnerID"] = "";
	$tdataverifica_voucher[".OriginalTable"] = "verifica_voucher";

//	field labels
$fieldLabelsverifica_voucher = array();
$fieldToolTipsverifica_voucher = array();
$pageTitlesverifica_voucher = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsverifica_voucher["Portuguese(Brazil)"] = array();
	$fieldToolTipsverifica_voucher["Portuguese(Brazil)"] = array();
	$pageTitlesverifica_voucher["Portuguese(Brazil)"] = array();
	$fieldLabelsverifica_voucher["Portuguese(Brazil)"]["id"] = "Id";
	$fieldToolTipsverifica_voucher["Portuguese(Brazil)"]["id"] = "";
	$fieldLabelsverifica_voucher["Portuguese(Brazil)"]["cpf"] = "CPF";
	$fieldToolTipsverifica_voucher["Portuguese(Brazil)"]["cpf"] = "";
	$fieldLabelsverifica_voucher["Portuguese(Brazil)"]["timestamp"] = "Timestamp";
	$fieldToolTipsverifica_voucher["Portuguese(Brazil)"]["timestamp"] = "";
	$pageTitlesverifica_voucher["Portuguese(Brazil)"]["add"] = "Verificar Voucher - Entrada";
	$pageTitlesverifica_voucher["Portuguese(Brazil)"]["list"] = "Entradas";
	$pageTitlesverifica_voucher["Portuguese(Brazil)"]["print"] = "Entradas";
	if (count($fieldToolTipsverifica_voucher["Portuguese(Brazil)"]))
		$tdataverifica_voucher[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsverifica_voucher[""] = array();
	$fieldToolTipsverifica_voucher[""] = array();
	$pageTitlesverifica_voucher[""] = array();
	$fieldLabelsverifica_voucher[""]["id"] = "Id";
	$fieldToolTipsverifica_voucher[""]["id"] = "";
	$fieldLabelsverifica_voucher[""]["cpf"] = "Cpf";
	$fieldToolTipsverifica_voucher[""]["cpf"] = "";
	$fieldLabelsverifica_voucher[""]["timestamp"] = "Timestamp";
	$fieldToolTipsverifica_voucher[""]["timestamp"] = "";
	if (count($fieldToolTipsverifica_voucher[""]))
		$tdataverifica_voucher[".isUseToolTips"] = true;
}


	$tdataverifica_voucher[".NCSearch"] = true;



$tdataverifica_voucher[".shortTableName"] = "verifica_voucher";
$tdataverifica_voucher[".nSecOptions"] = 0;
$tdataverifica_voucher[".recsPerRowList"] = 1;
$tdataverifica_voucher[".recsPerRowPrint"] = 1;
$tdataverifica_voucher[".mainTableOwnerID"] = "";
$tdataverifica_voucher[".moveNext"] = 1;
$tdataverifica_voucher[".entityType"] = 0;

$tdataverifica_voucher[".strOriginalTableName"] = "verifica_voucher";





$tdataverifica_voucher[".showAddInPopup"] = false;

$tdataverifica_voucher[".showEditInPopup"] = false;

$tdataverifica_voucher[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataverifica_voucher[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataverifica_voucher[".fieldsForRegister"] = array();

$tdataverifica_voucher[".listAjax"] = false;

	$tdataverifica_voucher[".audit"] = false;

	$tdataverifica_voucher[".locking"] = false;


$tdataverifica_voucher[".add"] = true;
$tdataverifica_voucher[".afterAddAction"] = 1;
$tdataverifica_voucher[".closePopupAfterAdd"] = 1;
$tdataverifica_voucher[".afterAddActionDetTable"] = "";

$tdataverifica_voucher[".list"] = true;

$tdataverifica_voucher[".view"] = true;


$tdataverifica_voucher[".exportTo"] = true;

$tdataverifica_voucher[".printFriendly"] = true;


$tdataverifica_voucher[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataverifica_voucher[".searchSaving"] = false;
//

$tdataverifica_voucher[".showSearchPanel"] = true;
		$tdataverifica_voucher[".flexibleSearch"] = true;

if (isMobile())
	$tdataverifica_voucher[".isUseAjaxSuggest"] = false;
else
	$tdataverifica_voucher[".isUseAjaxSuggest"] = true;

$tdataverifica_voucher[".rowHighlite"] = true;



$tdataverifica_voucher[".addPageEvents"] = false;

// use timepicker for search panel
$tdataverifica_voucher[".isUseTimeForSearch"] = false;



$tdataverifica_voucher[".badgeColor"] = "008B8B";


$tdataverifica_voucher[".allSearchFields"] = array();
$tdataverifica_voucher[".filterFields"] = array();
$tdataverifica_voucher[".requiredSearchFields"] = array();

$tdataverifica_voucher[".allSearchFields"][] = "cpf";
	$tdataverifica_voucher[".allSearchFields"][] = "timestamp";
	

$tdataverifica_voucher[".googleLikeFields"] = array();
$tdataverifica_voucher[".googleLikeFields"][] = "id";
$tdataverifica_voucher[".googleLikeFields"][] = "cpf";
$tdataverifica_voucher[".googleLikeFields"][] = "timestamp";


$tdataverifica_voucher[".advSearchFields"] = array();
$tdataverifica_voucher[".advSearchFields"][] = "id";
$tdataverifica_voucher[".advSearchFields"][] = "cpf";
$tdataverifica_voucher[".advSearchFields"][] = "timestamp";

$tdataverifica_voucher[".tableType"] = "list";

$tdataverifica_voucher[".printerPageOrientation"] = 0;
$tdataverifica_voucher[".nPrinterPageScale"] = 100;

$tdataverifica_voucher[".nPrinterSplitRecords"] = 40;

$tdataverifica_voucher[".nPrinterPDFSplitRecords"] = 40;



$tdataverifica_voucher[".geocodingEnabled"] = false;





$tdataverifica_voucher[".listGridLayout"] = 3;





// view page pdf

// print page pdf


$tdataverifica_voucher[".pageSize"] = 20;

$tdataverifica_voucher[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataverifica_voucher[".strOrderBy"] = $tstrOrderBy;

$tdataverifica_voucher[".orderindexes"] = array();

$tdataverifica_voucher[".sqlHead"] = "SELECT id,  	cpf,  	`timestamp`";
$tdataverifica_voucher[".sqlFrom"] = "FROM verifica_voucher";
$tdataverifica_voucher[".sqlWhereExpr"] = "";
$tdataverifica_voucher[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataverifica_voucher[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataverifica_voucher[".arrGroupsPerPage"] = $arrGPP;

$tdataverifica_voucher[".highlightSearchResults"] = true;

$tableKeysverifica_voucher = array();
$tableKeysverifica_voucher[] = "id";
$tdataverifica_voucher[".Keys"] = $tableKeysverifica_voucher;

$tdataverifica_voucher[".listFields"] = array();
$tdataverifica_voucher[".listFields"][] = "cpf";
$tdataverifica_voucher[".listFields"][] = "timestamp";

$tdataverifica_voucher[".hideMobileList"] = array();


$tdataverifica_voucher[".viewFields"] = array();
$tdataverifica_voucher[".viewFields"][] = "cpf";
$tdataverifica_voucher[".viewFields"][] = "timestamp";

$tdataverifica_voucher[".addFields"] = array();
$tdataverifica_voucher[".addFields"][] = "cpf";

$tdataverifica_voucher[".masterListFields"] = array();
$tdataverifica_voucher[".masterListFields"][] = "id";
$tdataverifica_voucher[".masterListFields"][] = "cpf";
$tdataverifica_voucher[".masterListFields"][] = "timestamp";

$tdataverifica_voucher[".inlineAddFields"] = array();

$tdataverifica_voucher[".editFields"] = array();

$tdataverifica_voucher[".inlineEditFields"] = array();

$tdataverifica_voucher[".exportFields"] = array();
$tdataverifica_voucher[".exportFields"][] = "cpf";
$tdataverifica_voucher[".exportFields"][] = "timestamp";

$tdataverifica_voucher[".importFields"] = array();

$tdataverifica_voucher[".printFields"] = array();
$tdataverifica_voucher[".printFields"][] = "cpf";
$tdataverifica_voucher[".printFields"][] = "timestamp";

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "verifica_voucher";
	$fdata["Label"] = GetFieldLabel("verifica_voucher","id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataverifica_voucher["id"] = $fdata;
//	cpf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "cpf";
	$fdata["GoodName"] = "cpf";
	$fdata["ownerTable"] = "verifica_voucher";
	$fdata["Label"] = GetFieldLabel("verifica_voucher","cpf");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cpf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cpf";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["strEditMask"] = "999.999.999-99";




		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdataverifica_voucher["cpf"] = $fdata;
//	timestamp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "timestamp";
	$fdata["GoodName"] = "timestamp";
	$fdata["ownerTable"] = "verifica_voucher";
	$fdata["Label"] = GetFieldLabel("verifica_voucher","timestamp");
	$fdata["FieldType"] = 135;

	
	
	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "timestamp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`timestamp`";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdataverifica_voucher["timestamp"] = $fdata;


$tables_data["verifica_voucher"]=&$tdataverifica_voucher;
$field_labels["verifica_voucher"] = &$fieldLabelsverifica_voucher;
$fieldToolTips["verifica_voucher"] = &$fieldToolTipsverifica_voucher;
$page_titles["verifica_voucher"] = &$pageTitlesverifica_voucher;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["verifica_voucher"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["verifica_voucher"] = array();


	
				$strOriginalDetailsTable="customers";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="customers";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "customers";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispMasterInfo"] = array();
				$masterParams["dispMasterInfo"][PAGE_LIST] = true;
			$masterParams["dispMasterInfo"][PAGE_PRINT] = true;
			
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["proceedLink"]= 1;

	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["verifica_voucher"][0] = $masterParams;
				$masterTablesData["verifica_voucher"][0]["masterKeys"] = array();
	$masterTablesData["verifica_voucher"][0]["masterKeys"][]="cpf";
				$masterTablesData["verifica_voucher"][0]["detailKeys"] = array();
	$masterTablesData["verifica_voucher"][0]["detailKeys"][]="cpf";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_verifica_voucher()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	cpf,  	`timestamp`";
$proto0["m_strFrom"] = "FROM verifica_voucher";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "verifica_voucher",
	"m_srcTableName" => "verifica_voucher"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "verifica_voucher";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "cpf",
	"m_strTable" => "verifica_voucher",
	"m_srcTableName" => "verifica_voucher"
));

$proto8["m_sql"] = "cpf";
$proto8["m_srcTableName"] = "verifica_voucher";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "timestamp",
	"m_strTable" => "verifica_voucher",
	"m_srcTableName" => "verifica_voucher"
));

$proto10["m_sql"] = "`timestamp`";
$proto10["m_srcTableName"] = "verifica_voucher";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "verifica_voucher";
$proto13["m_srcTableName"] = "verifica_voucher";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "id";
$proto13["m_columns"][] = "cpf";
$proto13["m_columns"][] = "timestamp";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "verifica_voucher";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "verifica_voucher";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="verifica_voucher";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_verifica_voucher = createSqlQuery_verifica_voucher();


	
		;

			

$tdataverifica_voucher[".sqlquery"] = $queryData_verifica_voucher;

include_once(getabspath("include/verifica_voucher_events.php"));
$tableEvents["verifica_voucher"] = new eventclass_verifica_voucher;
$tdataverifica_voucher[".hasEvents"] = true;

?>