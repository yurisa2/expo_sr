<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapesquisas_cpf = array();
	$tdatapesquisas_cpf[".truncateText"] = true;
	$tdatapesquisas_cpf[".NumberOfChars"] = 80;
	$tdatapesquisas_cpf[".ShortName"] = "pesquisas_cpf";
	$tdatapesquisas_cpf[".OwnerID"] = "";
	$tdatapesquisas_cpf[".OriginalTable"] = "pesquisas_cpf";

//	field labels
$fieldLabelspesquisas_cpf = array();
$fieldToolTipspesquisas_cpf = array();
$pageTitlespesquisas_cpf = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspesquisas_cpf["Portuguese(Brazil)"] = array();
	$fieldToolTipspesquisas_cpf["Portuguese(Brazil)"] = array();
	$pageTitlespesquisas_cpf["Portuguese(Brazil)"] = array();
	$fieldLabelspesquisas_cpf["Portuguese(Brazil)"]["id"] = "Id";
	$fieldToolTipspesquisas_cpf["Portuguese(Brazil)"]["id"] = "";
	$fieldLabelspesquisas_cpf["Portuguese(Brazil)"]["cpf"] = "CPF";
	$fieldToolTipspesquisas_cpf["Portuguese(Brazil)"]["cpf"] = "";
	$fieldLabelspesquisas_cpf["Portuguese(Brazil)"]["timestamp"] = "Timestamp";
	$fieldToolTipspesquisas_cpf["Portuguese(Brazil)"]["timestamp"] = "";
	$fieldLabelspesquisas_cpf["Portuguese(Brazil)"]["origem"] = "Origem";
	$fieldToolTipspesquisas_cpf["Portuguese(Brazil)"]["origem"] = "";
	$pageTitlespesquisas_cpf["Portuguese(Brazil)"]["add"] = "Digite seu CPF";
	if (count($fieldToolTipspesquisas_cpf["Portuguese(Brazil)"]))
		$tdatapesquisas_cpf[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspesquisas_cpf[""] = array();
	$fieldToolTipspesquisas_cpf[""] = array();
	$pageTitlespesquisas_cpf[""] = array();
	$fieldLabelspesquisas_cpf[""]["id"] = "Id";
	$fieldToolTipspesquisas_cpf[""]["id"] = "";
	$fieldLabelspesquisas_cpf[""]["cpf"] = "Cpf";
	$fieldToolTipspesquisas_cpf[""]["cpf"] = "";
	$fieldLabelspesquisas_cpf[""]["timestamp"] = "Timestamp";
	$fieldToolTipspesquisas_cpf[""]["timestamp"] = "";
	$fieldLabelspesquisas_cpf[""]["origem"] = "Origem";
	$fieldToolTipspesquisas_cpf[""]["origem"] = "";
	if (count($fieldToolTipspesquisas_cpf[""]))
		$tdatapesquisas_cpf[".isUseToolTips"] = true;
}


	$tdatapesquisas_cpf[".NCSearch"] = true;



$tdatapesquisas_cpf[".shortTableName"] = "pesquisas_cpf";
$tdatapesquisas_cpf[".nSecOptions"] = 0;
$tdatapesquisas_cpf[".recsPerRowList"] = 1;
$tdatapesquisas_cpf[".recsPerRowPrint"] = 1;
$tdatapesquisas_cpf[".mainTableOwnerID"] = "";
$tdatapesquisas_cpf[".moveNext"] = 1;
$tdatapesquisas_cpf[".entityType"] = 0;

$tdatapesquisas_cpf[".strOriginalTableName"] = "pesquisas_cpf";





$tdatapesquisas_cpf[".showAddInPopup"] = false;

$tdatapesquisas_cpf[".showEditInPopup"] = false;

$tdatapesquisas_cpf[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapesquisas_cpf[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapesquisas_cpf[".fieldsForRegister"] = array();

$tdatapesquisas_cpf[".listAjax"] = false;

	$tdatapesquisas_cpf[".audit"] = false;

	$tdatapesquisas_cpf[".locking"] = false;


$tdatapesquisas_cpf[".add"] = true;
$tdatapesquisas_cpf[".afterAddAction"] = 1;
$tdatapesquisas_cpf[".closePopupAfterAdd"] = 1;
$tdatapesquisas_cpf[".afterAddActionDetTable"] = "";







$tdatapesquisas_cpf[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatapesquisas_cpf[".searchSaving"] = false;
//

$tdatapesquisas_cpf[".showSearchPanel"] = true;
		$tdatapesquisas_cpf[".flexibleSearch"] = true;

if (isMobile())
	$tdatapesquisas_cpf[".isUseAjaxSuggest"] = false;
else
	$tdatapesquisas_cpf[".isUseAjaxSuggest"] = true;

$tdatapesquisas_cpf[".rowHighlite"] = true;



$tdatapesquisas_cpf[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapesquisas_cpf[".isUseTimeForSearch"] = false;



$tdatapesquisas_cpf[".badgeColor"] = "2F4F4F";


$tdatapesquisas_cpf[".allSearchFields"] = array();
$tdatapesquisas_cpf[".filterFields"] = array();
$tdatapesquisas_cpf[".requiredSearchFields"] = array();



$tdatapesquisas_cpf[".googleLikeFields"] = array();
$tdatapesquisas_cpf[".googleLikeFields"][] = "id";
$tdatapesquisas_cpf[".googleLikeFields"][] = "cpf";
$tdatapesquisas_cpf[".googleLikeFields"][] = "timestamp";
$tdatapesquisas_cpf[".googleLikeFields"][] = "origem";


$tdatapesquisas_cpf[".advSearchFields"] = array();
$tdatapesquisas_cpf[".advSearchFields"][] = "id";
$tdatapesquisas_cpf[".advSearchFields"][] = "cpf";
$tdatapesquisas_cpf[".advSearchFields"][] = "timestamp";
$tdatapesquisas_cpf[".advSearchFields"][] = "origem";

$tdatapesquisas_cpf[".tableType"] = "list";

$tdatapesquisas_cpf[".printerPageOrientation"] = 0;
$tdatapesquisas_cpf[".nPrinterPageScale"] = 100;

$tdatapesquisas_cpf[".nPrinterSplitRecords"] = 40;

$tdatapesquisas_cpf[".nPrinterPDFSplitRecords"] = 40;



$tdatapesquisas_cpf[".geocodingEnabled"] = false;





$tdatapesquisas_cpf[".listGridLayout"] = 3;





// view page pdf

// print page pdf


$tdatapesquisas_cpf[".pageSize"] = 20;

$tdatapesquisas_cpf[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapesquisas_cpf[".strOrderBy"] = $tstrOrderBy;

$tdatapesquisas_cpf[".orderindexes"] = array();

$tdatapesquisas_cpf[".sqlHead"] = "SELECT id,  	cpf,  	`timestamp`,  	origem";
$tdatapesquisas_cpf[".sqlFrom"] = "FROM pesquisas_cpf";
$tdatapesquisas_cpf[".sqlWhereExpr"] = "";
$tdatapesquisas_cpf[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapesquisas_cpf[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapesquisas_cpf[".arrGroupsPerPage"] = $arrGPP;

$tdatapesquisas_cpf[".highlightSearchResults"] = true;

$tableKeyspesquisas_cpf = array();
$tableKeyspesquisas_cpf[] = "id";
$tdatapesquisas_cpf[".Keys"] = $tableKeyspesquisas_cpf;

$tdatapesquisas_cpf[".listFields"] = array();

$tdatapesquisas_cpf[".hideMobileList"] = array();


$tdatapesquisas_cpf[".viewFields"] = array();

$tdatapesquisas_cpf[".addFields"] = array();
$tdatapesquisas_cpf[".addFields"][] = "cpf";

$tdatapesquisas_cpf[".masterListFields"] = array();
$tdatapesquisas_cpf[".masterListFields"][] = "id";
$tdatapesquisas_cpf[".masterListFields"][] = "cpf";
$tdatapesquisas_cpf[".masterListFields"][] = "timestamp";
$tdatapesquisas_cpf[".masterListFields"][] = "origem";

$tdatapesquisas_cpf[".inlineAddFields"] = array();

$tdatapesquisas_cpf[".editFields"] = array();

$tdatapesquisas_cpf[".inlineEditFields"] = array();

$tdatapesquisas_cpf[".exportFields"] = array();

$tdatapesquisas_cpf[".importFields"] = array();

$tdatapesquisas_cpf[".printFields"] = array();

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "pesquisas_cpf";
	$fdata["Label"] = GetFieldLabel("pesquisas_cpf","id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapesquisas_cpf["id"] = $fdata;
//	cpf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "cpf";
	$fdata["GoodName"] = "cpf";
	$fdata["ownerTable"] = "pesquisas_cpf";
	$fdata["Label"] = GetFieldLabel("pesquisas_cpf","cpf");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "cpf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cpf";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["strEditMask"] = "999.999.999-99";




	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapesquisas_cpf["cpf"] = $fdata;
//	timestamp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "timestamp";
	$fdata["GoodName"] = "timestamp";
	$fdata["ownerTable"] = "pesquisas_cpf";
	$fdata["Label"] = GetFieldLabel("pesquisas_cpf","timestamp");
	$fdata["FieldType"] = 135;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "timestamp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`timestamp`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapesquisas_cpf["timestamp"] = $fdata;
//	origem
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "origem";
	$fdata["GoodName"] = "origem";
	$fdata["ownerTable"] = "pesquisas_cpf";
	$fdata["Label"] = GetFieldLabel("pesquisas_cpf","origem");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "origem";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "origem";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapesquisas_cpf["origem"] = $fdata;


$tables_data["pesquisas_cpf"]=&$tdatapesquisas_cpf;
$field_labels["pesquisas_cpf"] = &$fieldLabelspesquisas_cpf;
$fieldToolTips["pesquisas_cpf"] = &$fieldToolTipspesquisas_cpf;
$page_titles["pesquisas_cpf"] = &$pageTitlespesquisas_cpf;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["pesquisas_cpf"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["pesquisas_cpf"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_pesquisas_cpf()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	cpf,  	`timestamp`,  	origem";
$proto0["m_strFrom"] = "FROM pesquisas_cpf";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "pesquisas_cpf",
	"m_srcTableName" => "pesquisas_cpf"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "pesquisas_cpf";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "cpf",
	"m_strTable" => "pesquisas_cpf",
	"m_srcTableName" => "pesquisas_cpf"
));

$proto8["m_sql"] = "cpf";
$proto8["m_srcTableName"] = "pesquisas_cpf";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "timestamp",
	"m_strTable" => "pesquisas_cpf",
	"m_srcTableName" => "pesquisas_cpf"
));

$proto10["m_sql"] = "`timestamp`";
$proto10["m_srcTableName"] = "pesquisas_cpf";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "origem",
	"m_strTable" => "pesquisas_cpf",
	"m_srcTableName" => "pesquisas_cpf"
));

$proto12["m_sql"] = "origem";
$proto12["m_srcTableName"] = "pesquisas_cpf";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto14=array();
$proto14["m_link"] = "SQLL_MAIN";
			$proto15=array();
$proto15["m_strName"] = "pesquisas_cpf";
$proto15["m_srcTableName"] = "pesquisas_cpf";
$proto15["m_columns"] = array();
$proto15["m_columns"][] = "id";
$proto15["m_columns"][] = "cpf";
$proto15["m_columns"][] = "timestamp";
$proto15["m_columns"][] = "origem";
$obj = new SQLTable($proto15);

$proto14["m_table"] = $obj;
$proto14["m_sql"] = "pesquisas_cpf";
$proto14["m_alias"] = "";
$proto14["m_srcTableName"] = "pesquisas_cpf";
$proto16=array();
$proto16["m_sql"] = "";
$proto16["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto16["m_column"]=$obj;
$proto16["m_contained"] = array();
$proto16["m_strCase"] = "";
$proto16["m_havingmode"] = false;
$proto16["m_inBrackets"] = false;
$proto16["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto16);

$proto14["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto14);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="pesquisas_cpf";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_pesquisas_cpf = createSqlQuery_pesquisas_cpf();


	
		;

				

$tdatapesquisas_cpf[".sqlquery"] = $queryData_pesquisas_cpf;

include_once(getabspath("include/pesquisas_cpf_events.php"));
$tableEvents["pesquisas_cpf"] = new eventclass_pesquisas_cpf;
$tdatapesquisas_cpf[".hasEvents"] = true;

?>