<?php

include '../VoucherCreator/output/custom/include/db_handle.php';

$db = new dbHandle;
$last_cpf = $db->last_cpf();
$info = $db->verify_cpf($last_cpf);

if($info) {
  $color_bg = "bg-success";
} else {
  $color_bg = "bg-danger";
}

// echo "<pre>";
//
// var_dump($info);
//
// echo $info->nome;
// echo $info->dt_nascimento;
// echo $info->cpf;
// echo $info->titulo_eleitor;
// echo $info->secao_eleitoral;


echo '

<script>

document.addEventListener(\'DOMContentLoaded\', (event) => {



var input = document.getElementById("value_cpf_1");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
   event.preventDefault();
   document.getElementById("saveButton1").click();
  }
});


document.getElementById(value_cpf_1).focus();
document.getElementById(value_cpf_1).select();

});

</script>

<style type="text/css">

#backToMenuButton1 {
 visibility: hidden !important;
}

#page-container {
  position: relative;
  min-height: 100vh;
}

#content-wrap {
  padding-bottom: 2.5rem;    /* Footer height */
}

#footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 20rem;            /* Footer height */
}

</style>

<div class="container '.$color_bg.'">
  <div class="row">
    <!--  Header Section-->
    <div class="row header">
      <div class="col-xs-12 text-center">
        <h4>Dados da última entrada: </h4>
      </div>
    </div>
    <div class="row content">
      <div class="col-md-4 col-sm-6 ">
      <h1>Nome: '.$info->nome.'</h1>
      </div>
      <div class="col-md-4 col-sm-6">
      <h1>CPF: '.$info->cpf.'</h1>
      </div>
      <div class="col-md-4 col-sm-6">
      <h1>RG: '.$info->rg.'</h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="row content">
    <div class="col-md-4 col-sm-6">
    Nascimento: '.$info->dt_nascimento.'
    </div>
      <div class="col-md-4 col-sm-6">
      Título de eleitor: '.$info->titulo_eleitor.'
      </div>
      <div class="col-md-4 col-sm-6">
      Seção Eleitoral: '.$info->secao_eleitoral.'
      </div>
    </div>
  </div>

  <div class="row">
    <div class="row content">
    <div class="col-md-4 col-sm-6">
    Endereco: '.$info->end.'
    </div>
      <div class="col-md-4 col-sm-6">
      num: '.$info->num.'
      </div>
      <div class="col-md-4 col-sm-6">
      Bairro: '.$info->bairro.'
      </div>
    </div>
  </div>

</div>


<footer id="footer">
  <div class="container">
    <div class="row">
      <!--  Header Section
      <div class="row header">
        <div class="col-xs-12 text-center">
          <h4>Contadores: </h4>
        </div>
      </div>-->
      <div class="row content">
        <div class="col-md-4 col-sm-6">
        <h4>HOJE:'.$db->total_checks_today().'</h4>
        </div>
        <div class="col-md-4 col-sm-6">
        <h4>TOTAL: '.$db->total_checks().'</h4>
        </div>
      </div>
    </div>
  </div>
</footer>










';

 ?>
