<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacustomers = array();
	$tdatacustomers[".truncateText"] = true;
	$tdatacustomers[".NumberOfChars"] = 80;
	$tdatacustomers[".ShortName"] = "customers";
	$tdatacustomers[".OwnerID"] = "";
	$tdatacustomers[".OriginalTable"] = "customers";

//	field labels
$fieldLabelscustomers = array();
$fieldToolTipscustomers = array();
$pageTitlescustomers = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelscustomers["Portuguese(Brazil)"] = array();
	$fieldToolTipscustomers["Portuguese(Brazil)"] = array();
	$pageTitlescustomers["Portuguese(Brazil)"] = array();
	$fieldLabelscustomers["Portuguese(Brazil)"]["id"] = "Id";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["id"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["dt_nascimento"] = "Data de nascimento";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["dt_nascimento"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["cpf"] = "CPF";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["cpf"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["titulo_eleitor"] = "Titulo de Eleitor";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["titulo_eleitor"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["zona_eleitoral"] = "Zona Eleitoral";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["zona_eleitoral"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["secao_eleitoral"] = "Seçao Eleitoral";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["secao_eleitoral"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["end"] = "Endereço";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["end"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["num"] = "Número";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["num"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["comp"] = "Complemento";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["comp"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["bairro"] = "Bairro";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["bairro"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["cidade"] = "Cidade";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["cidade"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["cep"] = "CEP";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["cep"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["uf"] = "UF";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["uf"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["fone"] = "Tel. Fixo";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["fone"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["fone1"] = "Tel. Celular";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["fone1"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["email"] = "Email";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["email"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["timestamp"] = "Registro";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["timestamp"] = "";
	$fieldLabelscustomers["Portuguese(Brazil)"]["rg"] = "RG";
	$fieldToolTipscustomers["Portuguese(Brazil)"]["rg"] = "";
	$pageTitlescustomers["Portuguese(Brazil)"]["add"] = "Cadastre-se para gerar o Voucher";
	if (count($fieldToolTipscustomers["Portuguese(Brazil)"]))
		$tdatacustomers[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscustomers[""] = array();
	$fieldToolTipscustomers[""] = array();
	$pageTitlescustomers[""] = array();
	$fieldLabelscustomers[""]["rg"] = "Rg";
	$fieldToolTipscustomers[""]["rg"] = "";
	if (count($fieldToolTipscustomers[""]))
		$tdatacustomers[".isUseToolTips"] = true;
}


	$tdatacustomers[".NCSearch"] = true;



$tdatacustomers[".shortTableName"] = "customers";
$tdatacustomers[".nSecOptions"] = 0;
$tdatacustomers[".recsPerRowList"] = 1;
$tdatacustomers[".recsPerRowPrint"] = 1;
$tdatacustomers[".mainTableOwnerID"] = "";
$tdatacustomers[".moveNext"] = 1;
$tdatacustomers[".entityType"] = 0;

$tdatacustomers[".strOriginalTableName"] = "customers";





$tdatacustomers[".showAddInPopup"] = false;

$tdatacustomers[".showEditInPopup"] = false;

$tdatacustomers[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacustomers[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacustomers[".fieldsForRegister"] = array();

$tdatacustomers[".listAjax"] = false;

	$tdatacustomers[".audit"] = false;

	$tdatacustomers[".locking"] = false;


$tdatacustomers[".add"] = true;
$tdatacustomers[".afterAddAction"] = 1;
$tdatacustomers[".closePopupAfterAdd"] = 1;
$tdatacustomers[".afterAddActionDetTable"] = "";







$tdatacustomers[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatacustomers[".searchSaving"] = false;
//

$tdatacustomers[".showSearchPanel"] = true;
		$tdatacustomers[".flexibleSearch"] = true;

if (isMobile())
	$tdatacustomers[".isUseAjaxSuggest"] = false;
else
	$tdatacustomers[".isUseAjaxSuggest"] = true;

$tdatacustomers[".rowHighlite"] = true;



$tdatacustomers[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacustomers[".isUseTimeForSearch"] = false;



$tdatacustomers[".badgeColor"] = "008B8B";


$tdatacustomers[".allSearchFields"] = array();
$tdatacustomers[".filterFields"] = array();
$tdatacustomers[".requiredSearchFields"] = array();



$tdatacustomers[".googleLikeFields"] = array();
$tdatacustomers[".googleLikeFields"][] = "id";
$tdatacustomers[".googleLikeFields"][] = "nome";
$tdatacustomers[".googleLikeFields"][] = "dt_nascimento";
$tdatacustomers[".googleLikeFields"][] = "cpf";
$tdatacustomers[".googleLikeFields"][] = "titulo_eleitor";
$tdatacustomers[".googleLikeFields"][] = "zona_eleitoral";
$tdatacustomers[".googleLikeFields"][] = "secao_eleitoral";
$tdatacustomers[".googleLikeFields"][] = "end";
$tdatacustomers[".googleLikeFields"][] = "num";
$tdatacustomers[".googleLikeFields"][] = "comp";
$tdatacustomers[".googleLikeFields"][] = "bairro";
$tdatacustomers[".googleLikeFields"][] = "cidade";
$tdatacustomers[".googleLikeFields"][] = "cep";
$tdatacustomers[".googleLikeFields"][] = "uf";
$tdatacustomers[".googleLikeFields"][] = "fone";
$tdatacustomers[".googleLikeFields"][] = "fone1";
$tdatacustomers[".googleLikeFields"][] = "email";
$tdatacustomers[".googleLikeFields"][] = "timestamp";
$tdatacustomers[".googleLikeFields"][] = "rg";


$tdatacustomers[".advSearchFields"] = array();
$tdatacustomers[".advSearchFields"][] = "id";
$tdatacustomers[".advSearchFields"][] = "timestamp";
$tdatacustomers[".advSearchFields"][] = "nome";
$tdatacustomers[".advSearchFields"][] = "dt_nascimento";
$tdatacustomers[".advSearchFields"][] = "rg";
$tdatacustomers[".advSearchFields"][] = "cpf";
$tdatacustomers[".advSearchFields"][] = "titulo_eleitor";
$tdatacustomers[".advSearchFields"][] = "zona_eleitoral";
$tdatacustomers[".advSearchFields"][] = "secao_eleitoral";
$tdatacustomers[".advSearchFields"][] = "end";
$tdatacustomers[".advSearchFields"][] = "num";
$tdatacustomers[".advSearchFields"][] = "comp";
$tdatacustomers[".advSearchFields"][] = "bairro";
$tdatacustomers[".advSearchFields"][] = "cidade";
$tdatacustomers[".advSearchFields"][] = "cep";
$tdatacustomers[".advSearchFields"][] = "uf";
$tdatacustomers[".advSearchFields"][] = "fone";
$tdatacustomers[".advSearchFields"][] = "fone1";
$tdatacustomers[".advSearchFields"][] = "email";

$tdatacustomers[".tableType"] = "list";

$tdatacustomers[".printerPageOrientation"] = 0;
$tdatacustomers[".nPrinterPageScale"] = 100;

$tdatacustomers[".nPrinterSplitRecords"] = 40;

$tdatacustomers[".nPrinterPDFSplitRecords"] = 40;



$tdatacustomers[".geocodingEnabled"] = false;





$tdatacustomers[".listGridLayout"] = 3;





// view page pdf

// print page pdf


$tdatacustomers[".pageSize"] = 20;

$tdatacustomers[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacustomers[".strOrderBy"] = $tstrOrderBy;

$tdatacustomers[".orderindexes"] = array();

$tdatacustomers[".sqlHead"] = "SELECT id,  nome,  dt_nascimento,  cpf,  titulo_eleitor,  zona_eleitoral,  secao_eleitoral,  `end`,  num,  comp,  bairro,  cidade,  cep,  uf,  fone,  fone1,  email,  `timestamp`,  rg";
$tdatacustomers[".sqlFrom"] = "FROM customers";
$tdatacustomers[".sqlWhereExpr"] = "";
$tdatacustomers[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacustomers[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacustomers[".arrGroupsPerPage"] = $arrGPP;

$tdatacustomers[".highlightSearchResults"] = true;

$tableKeyscustomers = array();
$tableKeyscustomers[] = "id";
$tdatacustomers[".Keys"] = $tableKeyscustomers;

$tdatacustomers[".listFields"] = array();

$tdatacustomers[".hideMobileList"] = array();


$tdatacustomers[".viewFields"] = array();

$tdatacustomers[".addFields"] = array();
$tdatacustomers[".addFields"][] = "timestamp";
$tdatacustomers[".addFields"][] = "nome";
$tdatacustomers[".addFields"][] = "dt_nascimento";
$tdatacustomers[".addFields"][] = "rg";
$tdatacustomers[".addFields"][] = "cpf";
$tdatacustomers[".addFields"][] = "titulo_eleitor";
$tdatacustomers[".addFields"][] = "zona_eleitoral";
$tdatacustomers[".addFields"][] = "secao_eleitoral";
$tdatacustomers[".addFields"][] = "end";
$tdatacustomers[".addFields"][] = "num";
$tdatacustomers[".addFields"][] = "comp";
$tdatacustomers[".addFields"][] = "bairro";
$tdatacustomers[".addFields"][] = "cidade";
$tdatacustomers[".addFields"][] = "cep";
$tdatacustomers[".addFields"][] = "uf";
$tdatacustomers[".addFields"][] = "fone";
$tdatacustomers[".addFields"][] = "fone1";
$tdatacustomers[".addFields"][] = "email";

$tdatacustomers[".masterListFields"] = array();
$tdatacustomers[".masterListFields"][] = "id";
$tdatacustomers[".masterListFields"][] = "timestamp";
$tdatacustomers[".masterListFields"][] = "nome";
$tdatacustomers[".masterListFields"][] = "dt_nascimento";
$tdatacustomers[".masterListFields"][] = "rg";
$tdatacustomers[".masterListFields"][] = "cpf";
$tdatacustomers[".masterListFields"][] = "titulo_eleitor";
$tdatacustomers[".masterListFields"][] = "zona_eleitoral";
$tdatacustomers[".masterListFields"][] = "secao_eleitoral";
$tdatacustomers[".masterListFields"][] = "end";
$tdatacustomers[".masterListFields"][] = "num";
$tdatacustomers[".masterListFields"][] = "comp";
$tdatacustomers[".masterListFields"][] = "bairro";
$tdatacustomers[".masterListFields"][] = "cidade";
$tdatacustomers[".masterListFields"][] = "cep";
$tdatacustomers[".masterListFields"][] = "uf";
$tdatacustomers[".masterListFields"][] = "fone";
$tdatacustomers[".masterListFields"][] = "fone1";
$tdatacustomers[".masterListFields"][] = "email";

$tdatacustomers[".inlineAddFields"] = array();

$tdatacustomers[".editFields"] = array();

$tdatacustomers[".inlineEditFields"] = array();

$tdatacustomers[".exportFields"] = array();

$tdatacustomers[".importFields"] = array();

$tdatacustomers[".printFields"] = array();

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["id"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","nome");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["nome"] = $fdata;
//	dt_nascimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "dt_nascimento";
	$fdata["GoodName"] = "dt_nascimento";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","dt_nascimento");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "dt_nascimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dt_nascimento";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 2;
	$edata["InitialYearFactor"] = 80;
	$edata["LastYearFactor"] = 0;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["dt_nascimento"] = $fdata;
//	cpf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "cpf";
	$fdata["GoodName"] = "cpf";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","cpf");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "cpf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cpf";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
		$edata["strEditMask"] = "999.999.999-99";




		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["cpf"] = $fdata;
//	titulo_eleitor
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "titulo_eleitor";
	$fdata["GoodName"] = "titulo_eleitor";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","titulo_eleitor");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "titulo_eleitor";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "titulo_eleitor";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["titulo_eleitor"] = $fdata;
//	zona_eleitoral
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "zona_eleitoral";
	$fdata["GoodName"] = "zona_eleitoral";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","zona_eleitoral");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "zona_eleitoral";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "zona_eleitoral";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["zona_eleitoral"] = $fdata;
//	secao_eleitoral
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "secao_eleitoral";
	$fdata["GoodName"] = "secao_eleitoral";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","secao_eleitoral");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "secao_eleitoral";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "secao_eleitoral";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["secao_eleitoral"] = $fdata;
//	end
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "end";
	$fdata["GoodName"] = "end";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","end");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "end";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`end`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["end"] = $fdata;
//	num
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "num";
	$fdata["GoodName"] = "num";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","num");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "num";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "num";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["num"] = $fdata;
//	comp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "comp";
	$fdata["GoodName"] = "comp";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","comp");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "comp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "comp";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["comp"] = $fdata;
//	bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "bairro";
	$fdata["GoodName"] = "bairro";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","bairro");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "bairro";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["bairro"] = $fdata;
//	cidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "cidade";
	$fdata["GoodName"] = "cidade";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","cidade");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "cidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cidade";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["cidade"] = $fdata;
//	cep
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "cep";
	$fdata["GoodName"] = "cep";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","cep");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "cep";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cep";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["strEditMask"] = "99999-999";




		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["cep"] = $fdata;
//	uf
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "uf";
	$fdata["GoodName"] = "uf";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","uf");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "uf";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "uf";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=2";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["uf"] = $fdata;
//	fone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "fone";
	$fdata["GoodName"] = "fone";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","fone");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "fone";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fone";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["strEditMask"] = "(99) 9999-9999";




	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["fone"] = $fdata;
//	fone1
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "fone1";
	$fdata["GoodName"] = "fone1";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","fone1");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "fone1";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fone1";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
		$edata["strEditMask"] = "(99) 99999-9999";




		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["fone1"] = $fdata;
//	email
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "email";
	$fdata["GoodName"] = "email";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","email");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "email";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "email";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Email");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["email"] = $fdata;
//	timestamp
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "timestamp";
	$fdata["GoodName"] = "timestamp";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","timestamp");
	$fdata["FieldType"] = 135;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "timestamp";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`timestamp`";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Readonly");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
		$edata["autoUpdatable"] = true;

	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["timestamp"] = $fdata;
//	rg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "rg";
	$fdata["GoodName"] = "rg";
	$fdata["ownerTable"] = "customers";
	$fdata["Label"] = GetFieldLabel("customers","rg");
	$fdata["FieldType"] = 200;

	
	
	
	
	
		$fdata["bAddPage"] = true;

	
	
	
	
	
	
	
		$fdata["strField"] = "rg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "rg";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatacustomers["rg"] = $fdata;


$tables_data["customers"]=&$tdatacustomers;
$field_labels["customers"] = &$fieldLabelscustomers;
$fieldToolTips["customers"] = &$fieldToolTipscustomers;
$page_titles["customers"] = &$pageTitlescustomers;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["customers"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["customers"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_customers()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  nome,  dt_nascimento,  cpf,  titulo_eleitor,  zona_eleitoral,  secao_eleitoral,  `end`,  num,  comp,  bairro,  cidade,  cep,  uf,  fone,  fone1,  email,  `timestamp`,  rg";
$proto0["m_strFrom"] = "FROM customers";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "customers";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto8["m_sql"] = "nome";
$proto8["m_srcTableName"] = "customers";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "dt_nascimento",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto10["m_sql"] = "dt_nascimento";
$proto10["m_srcTableName"] = "customers";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "cpf",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto12["m_sql"] = "cpf";
$proto12["m_srcTableName"] = "customers";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "titulo_eleitor",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto14["m_sql"] = "titulo_eleitor";
$proto14["m_srcTableName"] = "customers";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "zona_eleitoral",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto16["m_sql"] = "zona_eleitoral";
$proto16["m_srcTableName"] = "customers";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "secao_eleitoral",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto18["m_sql"] = "secao_eleitoral";
$proto18["m_srcTableName"] = "customers";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "end",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto20["m_sql"] = "`end`";
$proto20["m_srcTableName"] = "customers";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "num",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto22["m_sql"] = "num";
$proto22["m_srcTableName"] = "customers";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "comp",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto24["m_sql"] = "comp";
$proto24["m_srcTableName"] = "customers";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "bairro",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto26["m_sql"] = "bairro";
$proto26["m_srcTableName"] = "customers";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "cidade",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto28["m_sql"] = "cidade";
$proto28["m_srcTableName"] = "customers";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "cep",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto30["m_sql"] = "cep";
$proto30["m_srcTableName"] = "customers";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "uf",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto32["m_sql"] = "uf";
$proto32["m_srcTableName"] = "customers";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "fone",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto34["m_sql"] = "fone";
$proto34["m_srcTableName"] = "customers";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "fone1",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto36["m_sql"] = "fone1";
$proto36["m_srcTableName"] = "customers";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "email",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto38["m_sql"] = "email";
$proto38["m_srcTableName"] = "customers";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "timestamp",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto40["m_sql"] = "`timestamp`";
$proto40["m_srcTableName"] = "customers";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "rg",
	"m_strTable" => "customers",
	"m_srcTableName" => "customers"
));

$proto42["m_sql"] = "rg";
$proto42["m_srcTableName"] = "customers";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto44=array();
$proto44["m_link"] = "SQLL_MAIN";
			$proto45=array();
$proto45["m_strName"] = "customers";
$proto45["m_srcTableName"] = "customers";
$proto45["m_columns"] = array();
$proto45["m_columns"][] = "id";
$proto45["m_columns"][] = "nome";
$proto45["m_columns"][] = "dt_nascimento";
$proto45["m_columns"][] = "cpf";
$proto45["m_columns"][] = "titulo_eleitor";
$proto45["m_columns"][] = "zona_eleitoral";
$proto45["m_columns"][] = "secao_eleitoral";
$proto45["m_columns"][] = "end";
$proto45["m_columns"][] = "num";
$proto45["m_columns"][] = "comp";
$proto45["m_columns"][] = "bairro";
$proto45["m_columns"][] = "cidade";
$proto45["m_columns"][] = "cep";
$proto45["m_columns"][] = "uf";
$proto45["m_columns"][] = "fone";
$proto45["m_columns"][] = "fone1";
$proto45["m_columns"][] = "email";
$proto45["m_columns"][] = "timestamp";
$proto45["m_columns"][] = "rg";
$obj = new SQLTable($proto45);

$proto44["m_table"] = $obj;
$proto44["m_sql"] = "customers";
$proto44["m_alias"] = "";
$proto44["m_srcTableName"] = "customers";
$proto46=array();
$proto46["m_sql"] = "";
$proto46["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto46["m_column"]=$obj;
$proto46["m_contained"] = array();
$proto46["m_strCase"] = "";
$proto46["m_havingmode"] = false;
$proto46["m_inBrackets"] = false;
$proto46["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto46);

$proto44["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto44);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="customers";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_customers = createSqlQuery_customers();


	
		;

																			

$tdatacustomers[".sqlquery"] = $queryData_customers;

include_once(getabspath("include/customers_events.php"));
$tableEvents["customers"] = new eventclass_customers;
$tdatacustomers[".hasEvents"] = true;

?>