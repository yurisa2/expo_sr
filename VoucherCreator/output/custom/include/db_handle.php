<?php

class dbHandle {

  function __construct() {

    $this->conn_data = new StdClass;

    $conn_dtl = (array) new ConnectionManager();

    $conn_dtl = $conn_dtl["\0*\0_connectionsData"]["expo_sr_at_expo_sr_mysql_dbaas_com_br"]["connInfo"];

    $this->conn_data->srv = $conn_dtl[0];
    $this->conn_data->user = $conn_dtl[1];
    $this->conn_data->pass = $conn_dtl[2];
    $this->conn_data->db = $conn_dtl[1];

    $this->PDO = new PDO("mysql:host=".$this->conn_data->srv.";dbname=".$this->conn_data->db."",
                          $this->conn_data->user,
                          $this->conn_data->pass);
  }

  public function verify_cpf($cpf) {
    $stmt = $this->PDO->prepare('SELECT * FROM `customers` where cpf = "'.$cpf.'"');
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_OBJ);

    return $row;
  }

  public function last_cpf() {
    $stmt = $this->PDO->prepare('SELECT * FROM `verifica_voucher` order by id DESC');
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_OBJ);

    return $row->cpf;
  }

  public function total_checks() {
    $stmt = $this->PDO->prepare('SELECT count(*) as total FROM `verifica_voucher` order by id DESC');
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_OBJ);

    return $row->total;
  }

  public function total_checks_today() {
    $stmt = $this->PDO->prepare('SELECT count(*) as total FROM `verifica_voucher` where DATE(timestamp) = CURDATE()');
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_OBJ);

    return $row->total;
  }

  public function check_secao($cpf) {
    $stmt = $this->PDO->prepare('SELECT * FROM `secoes_autorizadas`  where secao = '.$this->verify_cpf($cpf)->secao_eleitoral);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_OBJ);

    return $row;
  }

  public function calc_index($cpf_obj) {
    $idx = 0;
    $multi = 0.9;

    if(strlen($cpf_obj->fone) > 4) $multi = 1;

      if($cpf_obj->zona_eleitoral == "131") $idx += 30;
      if($this->check_secao($cpf_obj->cpf)) $idx += 30;

      if(strpos($cpf_obj->fone,"(11)") !== FALSE)  $idx += 10;

      if(strpos(strtolower($cpf_obj->cidade),"roque") !== FALSE )  $idx += 10;

      if(strpos($cpf_obj->cep,"1813") !== FALSE )  $idx += 20;

      $idx = $idx / $multi;

      return $idx;
  }

}







 ?>
