<?php
include 'include/include_all.php';

$cpf_get = $_GET["cpf"];


$db = new dbHandle;
$cpf = $db->check_secao($cpf_get);
$full_cpf = $db->verify_cpf($cpf_get);


if($db->calc_index($full_cpf) < 70)
{
header("Location: error.php?titulo_erro=Dados Inconsistentes.&desc_erro=Houve um problema no cadastro, por favor, entre em contato com a organização através do e-mail contato@sindusvinho.com.br"); // RETORNA TRUE E FALSE
exit;
}

// echo '<pre>';
// var_dump($full_cpf);
// echo '<br>';
// var_dump($db->calc_index($full_cpf));

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Voucher Expo SR</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>@page { size: A5 landscape }</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A5 landscape" onload="window.print()">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm">

    <!-- Write HTML just like a web page -->
    <article>

      <center>
      <img src="images/image1.png"  class="col-md-2 col-sm-2">
      <?php
          $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
          // echo $generator->getBarcode($cpf_get, $generator::TYPE_CODE_128);
          echo '<img class="col-md-8 col-sm-8" src="data:image/png;base64,' . base64_encode($generator->getBarcode($cpf_get, $generator::TYPE_CODE_128)) . '">';
     ?>
     <img src="images/image3.png"  class="col-md-2 col-sm-2">

     <div class="container-fluid">
       <div class="row">

         <!--  Header Section-->
         <div class="row header">
           <div class="text-center col-md-12 col-sm-12">
             <h1><?php echo $full_cpf->nome; ?> </h1>
           </div>
         </div>

         <div class="row content">
           <div class="col-md-4 col-sm-4 ">
           <h4><small>CPF: </small><?php echo $full_cpf->cpf; ?></h4>
           </div>
           <div class="col-md-4 col-sm-4">
            <h4><small>RG: </small><?php echo $full_cpf->rg; ?></h4>
           </div>
           <div class="col-md-4 col-sm-4">
            <h4><small>Cel: </small> <?php echo $full_cpf->fone1; ?></h4>
           </div>
         </div>
       </div>

         <div class="row content">
           <div class="col-md-12 col-sm-12 ">
             <p>Você é o nosso convidado especial para visitar a 27ª Expo São Roque – Vinhos e Alcachofras.</p>
             <p>Para isso, apresente este VOUCHER na entrada do recinto sinalizada como: CREDENCIADOS. </p>
             <p>Sua Entrada será Gratuita! Venha. Aguardamos sua visita.</p>
           </div>
         </div>

         <div class="row content">
           <div class="col-md-12 col-sm-12 small ">
             <ul class="text-left">
             <p class="text-left">Atenção para as condições de uso do Voucher:</p>
               <li>Ele é exclusivo para moradores da Cidade de São Roque – SP;</li>
               <li> É válido para todos os dias da EXPO;</li>
               <li> É individual e intransferível;</li>
               <li> É Obrigatória apresentação de um documento com foto no momento do acesso;</li>
               <li> Apenas Vouchers impressos serão aceitos. Vouchers digitais, exibidos em telas de celular ou tablets, por exemplo, não serão aceitos;</li>
               <li> Menores de 18 anos, permitido a entrada somente acompanhada dos pais.</li>
               <li> No caso de perda deste voucher, o mesmo poderá ser reimpresso pelo site. Seu login é o seu CPF.</li>
             </ul>
           </div>
         </div>


         <div class="row content">
           <div class="col-md-12 col-sm-12 ">
             <p>A 27ª Expo São Roque – Vinhos e Alcachofras, acontece de 04 de outubro a 10 novembro de 2019, sexta a domingo – das 9 às 19 horas.</p>
             <p>Programação e mais informação da Festa acesse: www.exposaoroque.com.br.</p>
           </div>
         </div>


     </div>




   </center>

   </article>

  </section>

</body>

</html>
