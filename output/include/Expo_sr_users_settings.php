<?php
require_once(getabspath("classes/cipherer.php"));




$tdataExpo_sr_users = array();
	$tdataExpo_sr_users[".truncateText"] = true;
	$tdataExpo_sr_users[".NumberOfChars"] = 80;
	$tdataExpo_sr_users[".ShortName"] = "Expo_sr_users";
	$tdataExpo_sr_users[".OwnerID"] = "";
	$tdataExpo_sr_users[".OriginalTable"] = "Expo_sr_users";

//	field labels
$fieldLabelsExpo_sr_users = array();
$fieldToolTipsExpo_sr_users = array();
$pageTitlesExpo_sr_users = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsExpo_sr_users["Portuguese(Brazil)"] = array();
	$fieldToolTipsExpo_sr_users["Portuguese(Brazil)"] = array();
	$pageTitlesExpo_sr_users["Portuguese(Brazil)"] = array();
	$fieldLabelsExpo_sr_users["Portuguese(Brazil)"]["ID"] = "ID";
	$fieldToolTipsExpo_sr_users["Portuguese(Brazil)"]["ID"] = "";
	$fieldLabelsExpo_sr_users["Portuguese(Brazil)"]["username"] = "Username";
	$fieldToolTipsExpo_sr_users["Portuguese(Brazil)"]["username"] = "";
	$fieldLabelsExpo_sr_users["Portuguese(Brazil)"]["password"] = "Password";
	$fieldToolTipsExpo_sr_users["Portuguese(Brazil)"]["password"] = "";
	$fieldLabelsExpo_sr_users["Portuguese(Brazil)"]["email"] = "Email";
	$fieldToolTipsExpo_sr_users["Portuguese(Brazil)"]["email"] = "";
	$fieldLabelsExpo_sr_users["Portuguese(Brazil)"]["fullname"] = "Fullname";
	$fieldToolTipsExpo_sr_users["Portuguese(Brazil)"]["fullname"] = "";
	$fieldLabelsExpo_sr_users["Portuguese(Brazil)"]["groupid"] = "Groupid";
	$fieldToolTipsExpo_sr_users["Portuguese(Brazil)"]["groupid"] = "";
	$fieldLabelsExpo_sr_users["Portuguese(Brazil)"]["active"] = "Active";
	$fieldToolTipsExpo_sr_users["Portuguese(Brazil)"]["active"] = "";
	if (count($fieldToolTipsExpo_sr_users["Portuguese(Brazil)"]))
		$tdataExpo_sr_users[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsExpo_sr_users[""] = array();
	$fieldToolTipsExpo_sr_users[""] = array();
	$pageTitlesExpo_sr_users[""] = array();
	if (count($fieldToolTipsExpo_sr_users[""]))
		$tdataExpo_sr_users[".isUseToolTips"] = true;
}


	$tdataExpo_sr_users[".NCSearch"] = true;



$tdataExpo_sr_users[".shortTableName"] = "Expo_sr_users";
$tdataExpo_sr_users[".nSecOptions"] = 0;
$tdataExpo_sr_users[".recsPerRowList"] = 1;
$tdataExpo_sr_users[".recsPerRowPrint"] = 1;
$tdataExpo_sr_users[".mainTableOwnerID"] = "";
$tdataExpo_sr_users[".moveNext"] = 1;
$tdataExpo_sr_users[".entityType"] = 0;

$tdataExpo_sr_users[".strOriginalTableName"] = "Expo_sr_users";





$tdataExpo_sr_users[".showAddInPopup"] = false;

$tdataExpo_sr_users[".showEditInPopup"] = false;

$tdataExpo_sr_users[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataExpo_sr_users[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataExpo_sr_users[".fieldsForRegister"] = array();

$tdataExpo_sr_users[".listAjax"] = false;

	$tdataExpo_sr_users[".audit"] = true;

	$tdataExpo_sr_users[".locking"] = false;









$tdataExpo_sr_users[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataExpo_sr_users[".searchSaving"] = false;
//

$tdataExpo_sr_users[".showSearchPanel"] = true;
		$tdataExpo_sr_users[".flexibleSearch"] = true;

if (isMobile())
	$tdataExpo_sr_users[".isUseAjaxSuggest"] = false;
else
	$tdataExpo_sr_users[".isUseAjaxSuggest"] = true;

$tdataExpo_sr_users[".rowHighlite"] = true;



$tdataExpo_sr_users[".addPageEvents"] = false;

// use timepicker for search panel
$tdataExpo_sr_users[".isUseTimeForSearch"] = false;



$tdataExpo_sr_users[".badgeColor"] = "E07878";


$tdataExpo_sr_users[".allSearchFields"] = array();
$tdataExpo_sr_users[".filterFields"] = array();
$tdataExpo_sr_users[".requiredSearchFields"] = array();



$tdataExpo_sr_users[".googleLikeFields"] = array();
$tdataExpo_sr_users[".googleLikeFields"][] = "ID";
$tdataExpo_sr_users[".googleLikeFields"][] = "username";
$tdataExpo_sr_users[".googleLikeFields"][] = "password";
$tdataExpo_sr_users[".googleLikeFields"][] = "email";
$tdataExpo_sr_users[".googleLikeFields"][] = "fullname";
$tdataExpo_sr_users[".googleLikeFields"][] = "groupid";
$tdataExpo_sr_users[".googleLikeFields"][] = "active";


$tdataExpo_sr_users[".advSearchFields"] = array();
$tdataExpo_sr_users[".advSearchFields"][] = "ID";
$tdataExpo_sr_users[".advSearchFields"][] = "username";
$tdataExpo_sr_users[".advSearchFields"][] = "password";
$tdataExpo_sr_users[".advSearchFields"][] = "email";
$tdataExpo_sr_users[".advSearchFields"][] = "fullname";
$tdataExpo_sr_users[".advSearchFields"][] = "groupid";
$tdataExpo_sr_users[".advSearchFields"][] = "active";

$tdataExpo_sr_users[".tableType"] = "list";

$tdataExpo_sr_users[".printerPageOrientation"] = 0;
$tdataExpo_sr_users[".nPrinterPageScale"] = 100;

$tdataExpo_sr_users[".nPrinterSplitRecords"] = 40;

$tdataExpo_sr_users[".nPrinterPDFSplitRecords"] = 40;



$tdataExpo_sr_users[".geocodingEnabled"] = false;





$tdataExpo_sr_users[".listGridLayout"] = 3;





// view page pdf

// print page pdf


$tdataExpo_sr_users[".pageSize"] = 20;

$tdataExpo_sr_users[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataExpo_sr_users[".strOrderBy"] = $tstrOrderBy;

$tdataExpo_sr_users[".orderindexes"] = array();

$tdataExpo_sr_users[".sqlHead"] = "SELECT ID,  	username,  	password,  	email,  	fullname,  	groupid,  	active";
$tdataExpo_sr_users[".sqlFrom"] = "FROM Expo_sr_users";
$tdataExpo_sr_users[".sqlWhereExpr"] = "";
$tdataExpo_sr_users[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataExpo_sr_users[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataExpo_sr_users[".arrGroupsPerPage"] = $arrGPP;

$tdataExpo_sr_users[".highlightSearchResults"] = true;

$tableKeysExpo_sr_users = array();
$tableKeysExpo_sr_users[] = "ID";
$tdataExpo_sr_users[".Keys"] = $tableKeysExpo_sr_users;

$tdataExpo_sr_users[".listFields"] = array();

$tdataExpo_sr_users[".hideMobileList"] = array();


$tdataExpo_sr_users[".viewFields"] = array();

$tdataExpo_sr_users[".addFields"] = array();

$tdataExpo_sr_users[".masterListFields"] = array();
$tdataExpo_sr_users[".masterListFields"][] = "ID";
$tdataExpo_sr_users[".masterListFields"][] = "username";
$tdataExpo_sr_users[".masterListFields"][] = "password";
$tdataExpo_sr_users[".masterListFields"][] = "email";
$tdataExpo_sr_users[".masterListFields"][] = "fullname";
$tdataExpo_sr_users[".masterListFields"][] = "groupid";
$tdataExpo_sr_users[".masterListFields"][] = "active";

$tdataExpo_sr_users[".inlineAddFields"] = array();

$tdataExpo_sr_users[".editFields"] = array();

$tdataExpo_sr_users[".inlineEditFields"] = array();

$tdataExpo_sr_users[".exportFields"] = array();

$tdataExpo_sr_users[".importFields"] = array();

$tdataExpo_sr_users[".printFields"] = array();

//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "Expo_sr_users";
	$fdata["Label"] = GetFieldLabel("Expo_sr_users","ID");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataExpo_sr_users["ID"] = $fdata;
//	username
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "username";
	$fdata["GoodName"] = "username";
	$fdata["ownerTable"] = "Expo_sr_users";
	$fdata["Label"] = GetFieldLabel("Expo_sr_users","username");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "username";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "username";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataExpo_sr_users["username"] = $fdata;
//	password
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "password";
	$fdata["GoodName"] = "password";
	$fdata["ownerTable"] = "Expo_sr_users";
	$fdata["Label"] = GetFieldLabel("Expo_sr_users","password");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "password";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "password";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Password");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataExpo_sr_users["password"] = $fdata;
//	email
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "email";
	$fdata["GoodName"] = "email";
	$fdata["ownerTable"] = "Expo_sr_users";
	$fdata["Label"] = GetFieldLabel("Expo_sr_users","email");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "email";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "email";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataExpo_sr_users["email"] = $fdata;
//	fullname
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "fullname";
	$fdata["GoodName"] = "fullname";
	$fdata["ownerTable"] = "Expo_sr_users";
	$fdata["Label"] = GetFieldLabel("Expo_sr_users","fullname");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "fullname";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fullname";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataExpo_sr_users["fullname"] = $fdata;
//	groupid
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "groupid";
	$fdata["GoodName"] = "groupid";
	$fdata["ownerTable"] = "Expo_sr_users";
	$fdata["Label"] = GetFieldLabel("Expo_sr_users","groupid");
	$fdata["FieldType"] = 200;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "groupid";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "groupid";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataExpo_sr_users["groupid"] = $fdata;
//	active
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "active";
	$fdata["GoodName"] = "active";
	$fdata["ownerTable"] = "Expo_sr_users";
	$fdata["Label"] = GetFieldLabel("Expo_sr_users","active");
	$fdata["FieldType"] = 3;

	
	
	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "active";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "active";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdataExpo_sr_users["active"] = $fdata;


$tables_data["Expo_sr_users"]=&$tdataExpo_sr_users;
$field_labels["Expo_sr_users"] = &$fieldLabelsExpo_sr_users;
$fieldToolTips["Expo_sr_users"] = &$fieldToolTipsExpo_sr_users;
$page_titles["Expo_sr_users"] = &$pageTitlesExpo_sr_users;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["Expo_sr_users"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["Expo_sr_users"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_Expo_sr_users()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  	username,  	password,  	email,  	fullname,  	groupid,  	active";
$proto0["m_strFrom"] = "FROM Expo_sr_users";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "Expo_sr_users",
	"m_srcTableName" => "Expo_sr_users"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "Expo_sr_users";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "username",
	"m_strTable" => "Expo_sr_users",
	"m_srcTableName" => "Expo_sr_users"
));

$proto8["m_sql"] = "username";
$proto8["m_srcTableName"] = "Expo_sr_users";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "password",
	"m_strTable" => "Expo_sr_users",
	"m_srcTableName" => "Expo_sr_users"
));

$proto10["m_sql"] = "password";
$proto10["m_srcTableName"] = "Expo_sr_users";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "email",
	"m_strTable" => "Expo_sr_users",
	"m_srcTableName" => "Expo_sr_users"
));

$proto12["m_sql"] = "email";
$proto12["m_srcTableName"] = "Expo_sr_users";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "fullname",
	"m_strTable" => "Expo_sr_users",
	"m_srcTableName" => "Expo_sr_users"
));

$proto14["m_sql"] = "fullname";
$proto14["m_srcTableName"] = "Expo_sr_users";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "groupid",
	"m_strTable" => "Expo_sr_users",
	"m_srcTableName" => "Expo_sr_users"
));

$proto16["m_sql"] = "groupid";
$proto16["m_srcTableName"] = "Expo_sr_users";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "active",
	"m_strTable" => "Expo_sr_users",
	"m_srcTableName" => "Expo_sr_users"
));

$proto18["m_sql"] = "active";
$proto18["m_srcTableName"] = "Expo_sr_users";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "Expo_sr_users";
$proto21["m_srcTableName"] = "Expo_sr_users";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "ID";
$proto21["m_columns"][] = "username";
$proto21["m_columns"][] = "password";
$proto21["m_columns"][] = "email";
$proto21["m_columns"][] = "fullname";
$proto21["m_columns"][] = "groupid";
$proto21["m_columns"][] = "active";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "Expo_sr_users";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "Expo_sr_users";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="Expo_sr_users";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_Expo_sr_users = createSqlQuery_Expo_sr_users();


	
		;

							

$tdataExpo_sr_users[".sqlquery"] = $queryData_Expo_sr_users;

$tableEvents["Expo_sr_users"] = new eventsBase;
$tdataExpo_sr_users[".hasEvents"] = false;

?>