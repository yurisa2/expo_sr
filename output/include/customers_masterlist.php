<?php
function DisplayMasterTableInfo_customers($params)
{
	$keys = $params["keys"];
	$detailtable = $params["detailtable"];
	$data = $params["masterRecordData"];
	
	$xt = new Xtempl();
	$tName = "customers";
	$xt->eventsObject = getEventObject($tName);
	
	include_once(getabspath('classes/listpage.php'));
	include_once(getabspath('classes/listpage_simple.php'));
	$mParams  = array();
	$mParams["xt"] = &$xt;
	$mParams["mode"] = LIST_MASTER;
	$mParams["pageType"] = PAGE_LIST;
	$mParams["flyId"] = $params["recId"];
	$masterPage = ListPage::createListPage($tName, $mParams);
	
	$settings = $masterPage->pSet;
	$viewControls = new ViewControlsContainer($settings, PAGE_LIST, $masterPage);
	
	$keysAssoc = array();
	$showKeys = "";	

	if($detailtable == "verifica_voucher")
	{
		$keysAssoc["cpf"] = $keys[1-1];
				
				$keyValue = $viewControls->showDBValue("cpf", $keysAssoc);
		$showKeys.= " ".GetFieldLabel("customers","cpf").": ".$keyValue;
		$xt->assign('showKeys', $showKeys);
	}

	if( !$data || !count($data) )
		return;
	
	// reassign pagetitlelabel function adding extra params
	$xt->assign_function("pagetitlelabel", "xt_pagetitlelabel", array("record" => $data, "settings" => $settings));
	
	$keylink = "";
	$keylink.= "&key1=".runner_htmlspecialchars(rawurlencode(@$data["id"]));
	
	$xt->assign("nome_mastervalue", $viewControls->showDBValue("nome", $data, $keylink));
	$format = $settings->getViewFormat("nome");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("nome")))
		$class = ' rnr-field-number';
		
	$xt->assign("nome_class", $class); // add class for field header as field value
	$xt->assign("dt_nascimento_mastervalue", $viewControls->showDBValue("dt_nascimento", $data, $keylink));
	$format = $settings->getViewFormat("dt_nascimento");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("dt_nascimento")))
		$class = ' rnr-field-number';
		
	$xt->assign("dt_nascimento_class", $class); // add class for field header as field value
	$xt->assign("cpf_mastervalue", $viewControls->showDBValue("cpf", $data, $keylink));
	$format = $settings->getViewFormat("cpf");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("cpf")))
		$class = ' rnr-field-number';
		
	$xt->assign("cpf_class", $class); // add class for field header as field value
	$xt->assign("titulo_eleitor_mastervalue", $viewControls->showDBValue("titulo_eleitor", $data, $keylink));
	$format = $settings->getViewFormat("titulo_eleitor");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("titulo_eleitor")))
		$class = ' rnr-field-number';
		
	$xt->assign("titulo_eleitor_class", $class); // add class for field header as field value
	$xt->assign("zona_eleitoral_mastervalue", $viewControls->showDBValue("zona_eleitoral", $data, $keylink));
	$format = $settings->getViewFormat("zona_eleitoral");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("zona_eleitoral")))
		$class = ' rnr-field-number';
		
	$xt->assign("zona_eleitoral_class", $class); // add class for field header as field value
	$xt->assign("secao_eleitoral_mastervalue", $viewControls->showDBValue("secao_eleitoral", $data, $keylink));
	$format = $settings->getViewFormat("secao_eleitoral");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("secao_eleitoral")))
		$class = ' rnr-field-number';
		
	$xt->assign("secao_eleitoral_class", $class); // add class for field header as field value
	$xt->assign("end_mastervalue", $viewControls->showDBValue("end", $data, $keylink));
	$format = $settings->getViewFormat("end");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("end")))
		$class = ' rnr-field-number';
		
	$xt->assign("end_class", $class); // add class for field header as field value
	$xt->assign("num_mastervalue", $viewControls->showDBValue("num", $data, $keylink));
	$format = $settings->getViewFormat("num");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("num")))
		$class = ' rnr-field-number';
		
	$xt->assign("num_class", $class); // add class for field header as field value
	$xt->assign("comp_mastervalue", $viewControls->showDBValue("comp", $data, $keylink));
	$format = $settings->getViewFormat("comp");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("comp")))
		$class = ' rnr-field-number';
		
	$xt->assign("comp_class", $class); // add class for field header as field value
	$xt->assign("bairro_mastervalue", $viewControls->showDBValue("bairro", $data, $keylink));
	$format = $settings->getViewFormat("bairro");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("bairro")))
		$class = ' rnr-field-number';
		
	$xt->assign("bairro_class", $class); // add class for field header as field value
	$xt->assign("cidade_mastervalue", $viewControls->showDBValue("cidade", $data, $keylink));
	$format = $settings->getViewFormat("cidade");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("cidade")))
		$class = ' rnr-field-number';
		
	$xt->assign("cidade_class", $class); // add class for field header as field value
	$xt->assign("cep_mastervalue", $viewControls->showDBValue("cep", $data, $keylink));
	$format = $settings->getViewFormat("cep");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("cep")))
		$class = ' rnr-field-number';
		
	$xt->assign("cep_class", $class); // add class for field header as field value
	$xt->assign("uf_mastervalue", $viewControls->showDBValue("uf", $data, $keylink));
	$format = $settings->getViewFormat("uf");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("uf")))
		$class = ' rnr-field-number';
		
	$xt->assign("uf_class", $class); // add class for field header as field value
	$xt->assign("fone_mastervalue", $viewControls->showDBValue("fone", $data, $keylink));
	$format = $settings->getViewFormat("fone");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("fone")))
		$class = ' rnr-field-number';
		
	$xt->assign("fone_class", $class); // add class for field header as field value
	$xt->assign("fone1_mastervalue", $viewControls->showDBValue("fone1", $data, $keylink));
	$format = $settings->getViewFormat("fone1");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("fone1")))
		$class = ' rnr-field-number';
		
	$xt->assign("fone1_class", $class); // add class for field header as field value
	$xt->assign("email_mastervalue", $viewControls->showDBValue("email", $data, $keylink));
	$format = $settings->getViewFormat("email");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("email")))
		$class = ' rnr-field-number';
		
	$xt->assign("email_class", $class); // add class for field header as field value
	$xt->assign("timestamp_mastervalue", $viewControls->showDBValue("timestamp", $data, $keylink));
	$format = $settings->getViewFormat("timestamp");
	$class = " rnr-field-text";
	if($format == FORMAT_FILE) 
		$class = ' rnr-field-file'; 
	if($format == FORMAT_AUDIO)
		$class = ' rnr-field-audio';
	if($format == FORMAT_CHECKBOX)
		$class = ' rnr-field-checkbox';
	if($format == FORMAT_NUMBER || IsNumberType($settings->getFieldType("timestamp")))
		$class = ' rnr-field-number';
		
	$xt->assign("timestamp_class", $class); // add class for field header as field value

	$layout = GetPageLayout("customers", 'masterlist');
	if( $layout )
		$xt->assign("pageattrs", 'class="'.$layout->style." page-".$layout->name.'"');
	
	$xt->displayPartial(GetTemplateName("customers", "masterlist"));
}

?>