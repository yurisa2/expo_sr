<?php
require_once(getabspath("classes/cipherer.php"));




$tdatasecoes_autorizadas = array();
	$tdatasecoes_autorizadas[".truncateText"] = true;
	$tdatasecoes_autorizadas[".NumberOfChars"] = 80;
	$tdatasecoes_autorizadas[".ShortName"] = "secoes_autorizadas";
	$tdatasecoes_autorizadas[".OwnerID"] = "";
	$tdatasecoes_autorizadas[".OriginalTable"] = "secoes_autorizadas";

//	field labels
$fieldLabelssecoes_autorizadas = array();
$fieldToolTipssecoes_autorizadas = array();
$pageTitlessecoes_autorizadas = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelssecoes_autorizadas["Portuguese(Brazil)"] = array();
	$fieldToolTipssecoes_autorizadas["Portuguese(Brazil)"] = array();
	$pageTitlessecoes_autorizadas["Portuguese(Brazil)"] = array();
	$fieldLabelssecoes_autorizadas["Portuguese(Brazil)"]["ID"] = "ID";
	$fieldToolTipssecoes_autorizadas["Portuguese(Brazil)"]["ID"] = "";
	$fieldLabelssecoes_autorizadas["Portuguese(Brazil)"]["secao"] = "Secao";
	$fieldToolTipssecoes_autorizadas["Portuguese(Brazil)"]["secao"] = "";
	if (count($fieldToolTipssecoes_autorizadas["Portuguese(Brazil)"]))
		$tdatasecoes_autorizadas[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelssecoes_autorizadas[""] = array();
	$fieldToolTipssecoes_autorizadas[""] = array();
	$pageTitlessecoes_autorizadas[""] = array();
	$fieldLabelssecoes_autorizadas[""]["secao"] = "Secao";
	$fieldToolTipssecoes_autorizadas[""]["secao"] = "";
	if (count($fieldToolTipssecoes_autorizadas[""]))
		$tdatasecoes_autorizadas[".isUseToolTips"] = true;
}


	$tdatasecoes_autorizadas[".NCSearch"] = true;



$tdatasecoes_autorizadas[".shortTableName"] = "secoes_autorizadas";
$tdatasecoes_autorizadas[".nSecOptions"] = 0;
$tdatasecoes_autorizadas[".recsPerRowList"] = 1;
$tdatasecoes_autorizadas[".recsPerRowPrint"] = 1;
$tdatasecoes_autorizadas[".mainTableOwnerID"] = "";
$tdatasecoes_autorizadas[".moveNext"] = 1;
$tdatasecoes_autorizadas[".entityType"] = 0;

$tdatasecoes_autorizadas[".strOriginalTableName"] = "secoes_autorizadas";





$tdatasecoes_autorizadas[".showAddInPopup"] = false;

$tdatasecoes_autorizadas[".showEditInPopup"] = false;

$tdatasecoes_autorizadas[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatasecoes_autorizadas[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatasecoes_autorizadas[".fieldsForRegister"] = array();

$tdatasecoes_autorizadas[".listAjax"] = false;

	$tdatasecoes_autorizadas[".audit"] = true;

	$tdatasecoes_autorizadas[".locking"] = false;

$tdatasecoes_autorizadas[".edit"] = true;
$tdatasecoes_autorizadas[".afterEditAction"] = 1;
$tdatasecoes_autorizadas[".closePopupAfterEdit"] = 1;
$tdatasecoes_autorizadas[".afterEditActionDetTable"] = "";

$tdatasecoes_autorizadas[".add"] = true;
$tdatasecoes_autorizadas[".afterAddAction"] = 1;
$tdatasecoes_autorizadas[".closePopupAfterAdd"] = 1;
$tdatasecoes_autorizadas[".afterAddActionDetTable"] = "";

$tdatasecoes_autorizadas[".list"] = true;

$tdatasecoes_autorizadas[".view"] = true;

$tdatasecoes_autorizadas[".import"] = true;

$tdatasecoes_autorizadas[".exportTo"] = true;

$tdatasecoes_autorizadas[".printFriendly"] = true;

$tdatasecoes_autorizadas[".delete"] = true;

$tdatasecoes_autorizadas[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatasecoes_autorizadas[".searchSaving"] = false;
//

$tdatasecoes_autorizadas[".showSearchPanel"] = true;
		$tdatasecoes_autorizadas[".flexibleSearch"] = true;

if (isMobile())
	$tdatasecoes_autorizadas[".isUseAjaxSuggest"] = false;
else
	$tdatasecoes_autorizadas[".isUseAjaxSuggest"] = true;

$tdatasecoes_autorizadas[".rowHighlite"] = true;



$tdatasecoes_autorizadas[".addPageEvents"] = false;

// use timepicker for search panel
$tdatasecoes_autorizadas[".isUseTimeForSearch"] = false;



$tdatasecoes_autorizadas[".badgeColor"] = "9ACD32";


$tdatasecoes_autorizadas[".allSearchFields"] = array();
$tdatasecoes_autorizadas[".filterFields"] = array();
$tdatasecoes_autorizadas[".requiredSearchFields"] = array();

$tdatasecoes_autorizadas[".allSearchFields"][] = "secao";
	

$tdatasecoes_autorizadas[".googleLikeFields"] = array();
$tdatasecoes_autorizadas[".googleLikeFields"][] = "ID";
$tdatasecoes_autorizadas[".googleLikeFields"][] = "secao";


$tdatasecoes_autorizadas[".advSearchFields"] = array();
$tdatasecoes_autorizadas[".advSearchFields"][] = "ID";
$tdatasecoes_autorizadas[".advSearchFields"][] = "secao";

$tdatasecoes_autorizadas[".tableType"] = "list";

$tdatasecoes_autorizadas[".printerPageOrientation"] = 0;
$tdatasecoes_autorizadas[".nPrinterPageScale"] = 100;

$tdatasecoes_autorizadas[".nPrinterSplitRecords"] = 40;

$tdatasecoes_autorizadas[".nPrinterPDFSplitRecords"] = 40;



$tdatasecoes_autorizadas[".geocodingEnabled"] = false;





$tdatasecoes_autorizadas[".listGridLayout"] = 3;





// view page pdf

// print page pdf


$tdatasecoes_autorizadas[".pageSize"] = 20;

$tdatasecoes_autorizadas[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatasecoes_autorizadas[".strOrderBy"] = $tstrOrderBy;

$tdatasecoes_autorizadas[".orderindexes"] = array();

$tdatasecoes_autorizadas[".sqlHead"] = "SELECT ID,  secao";
$tdatasecoes_autorizadas[".sqlFrom"] = "FROM secoes_autorizadas";
$tdatasecoes_autorizadas[".sqlWhereExpr"] = "";
$tdatasecoes_autorizadas[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatasecoes_autorizadas[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatasecoes_autorizadas[".arrGroupsPerPage"] = $arrGPP;

$tdatasecoes_autorizadas[".highlightSearchResults"] = true;

$tableKeyssecoes_autorizadas = array();
$tableKeyssecoes_autorizadas[] = "ID";
$tdatasecoes_autorizadas[".Keys"] = $tableKeyssecoes_autorizadas;

$tdatasecoes_autorizadas[".listFields"] = array();
$tdatasecoes_autorizadas[".listFields"][] = "secao";

$tdatasecoes_autorizadas[".hideMobileList"] = array();


$tdatasecoes_autorizadas[".viewFields"] = array();
$tdatasecoes_autorizadas[".viewFields"][] = "secao";

$tdatasecoes_autorizadas[".addFields"] = array();
$tdatasecoes_autorizadas[".addFields"][] = "secao";

$tdatasecoes_autorizadas[".masterListFields"] = array();
$tdatasecoes_autorizadas[".masterListFields"][] = "ID";
$tdatasecoes_autorizadas[".masterListFields"][] = "secao";

$tdatasecoes_autorizadas[".inlineAddFields"] = array();

$tdatasecoes_autorizadas[".editFields"] = array();
$tdatasecoes_autorizadas[".editFields"][] = "secao";

$tdatasecoes_autorizadas[".inlineEditFields"] = array();

$tdatasecoes_autorizadas[".exportFields"] = array();
$tdatasecoes_autorizadas[".exportFields"][] = "secao";

$tdatasecoes_autorizadas[".importFields"] = array();
$tdatasecoes_autorizadas[".importFields"][] = "secao";

$tdatasecoes_autorizadas[".printFields"] = array();
$tdatasecoes_autorizadas[".printFields"][] = "secao";

//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "secoes_autorizadas";
	$fdata["Label"] = GetFieldLabel("secoes_autorizadas","ID");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ID";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatasecoes_autorizadas["ID"] = $fdata;
//	secao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "secao";
	$fdata["GoodName"] = "secao";
	$fdata["ownerTable"] = "secoes_autorizadas";
	$fdata["Label"] = GetFieldLabel("secoes_autorizadas","secao");
	$fdata["FieldType"] = 200;

	
	
	
	
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "secao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "secao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatasecoes_autorizadas["secao"] = $fdata;


$tables_data["secoes_autorizadas"]=&$tdatasecoes_autorizadas;
$field_labels["secoes_autorizadas"] = &$fieldLabelssecoes_autorizadas;
$fieldToolTips["secoes_autorizadas"] = &$fieldToolTipssecoes_autorizadas;
$page_titles["secoes_autorizadas"] = &$pageTitlessecoes_autorizadas;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["secoes_autorizadas"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["secoes_autorizadas"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_secoes_autorizadas()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ID,  secao";
$proto0["m_strFrom"] = "FROM secoes_autorizadas";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "secoes_autorizadas",
	"m_srcTableName" => "secoes_autorizadas"
));

$proto6["m_sql"] = "ID";
$proto6["m_srcTableName"] = "secoes_autorizadas";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "secao",
	"m_strTable" => "secoes_autorizadas",
	"m_srcTableName" => "secoes_autorizadas"
));

$proto8["m_sql"] = "secao";
$proto8["m_srcTableName"] = "secoes_autorizadas";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "secoes_autorizadas";
$proto11["m_srcTableName"] = "secoes_autorizadas";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "ID";
$proto11["m_columns"][] = "secao";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "secoes_autorizadas";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "secoes_autorizadas";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="secoes_autorizadas";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_secoes_autorizadas = createSqlQuery_secoes_autorizadas();


	
		;

		

$tdatasecoes_autorizadas[".sqlquery"] = $queryData_secoes_autorizadas;

$tableEvents["secoes_autorizadas"] = new eventsBase;
$tdatasecoes_autorizadas[".hasEvents"] = false;

?>